﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace JobPostingManagementSystem.App_Code
{
    public class AppConfiguration
    {
        public AppConfiguration()
        { 
        }

        public static string GetConnectionString()
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
            return connString;
        }

    }
}