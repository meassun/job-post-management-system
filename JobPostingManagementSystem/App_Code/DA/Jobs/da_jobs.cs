﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace JobPostingManagementSystem.App_Code.DA.Jobs
{
    class da_Jobs
    {
        SqlDataAdapter my_da;

        public da_Jobs()
        {

        }

        public  DataTable GetTotalAllJob()
        {
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "JP_GET_TOTAL_JOB";
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();
            }
            return dt;
        }

    }
}
