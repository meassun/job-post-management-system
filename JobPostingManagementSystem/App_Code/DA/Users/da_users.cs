﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace JobPostingManagementSystem.App_Code.DA.Users
{
    class da_users
    {

        public da_users()
        {
            
        }

        public static bool GetUserExistingRegister(string username, string email)
        {
            bool status = false;

            //Create new customer and get Customer_ID  
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "JP_SP_GET_USER_BY_USERNAME";
                cmd.Parameters.AddWithValue("@Username", username);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Connection = con;
                con.Open();
                try
                {
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                        return !status;
                    else
                        return status;
                }

                catch (Exception ex)
                {
                    return status;
                }
                con.Close();
            }
        }

        public static bool AddNewUser(string fullname, string username, string email, string password, string image, string role)
        {
            bool status = false;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_USER_ADD";
                    cmd.Parameters.Add("@Fullname", SqlDbType.NVarChar).Value = fullname;
                    cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = username;
                    cmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = email;
                    cmd.Parameters.Add("@Pwd", SqlDbType.NVarChar).Value = password;
                    cmd.Parameters.Add("@Profiles", SqlDbType.NVarChar).Value = image;
                    cmd.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = role;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }
            
      
        }
        public static DataTable GetUserLogIn(string username, string password)
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "JP_SP_GET_USER_BY_LOGIN";
                cmd.Parameters.AddWithValue("@Username", username);
                cmd.Parameters.AddWithValue("@Pwd", password);
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();
            
                return dt;
            }
        }

        public static bool UpdateUser(string userid, string username, string email)
        { 
            bool status = false;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_UPDATE_USER_PROFILE";
                    cmd.Parameters.AddWithValue("@userid", SqlDbType.NVarChar).Value = userid;
                    cmd.Parameters.AddWithValue("@username", SqlDbType.NVarChar).Value = username;
                    cmd.Parameters.AddWithValue("@email", SqlDbType.NVarChar).Value = email;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }

        }

        public static DataTable GetTotalCurrentPostJob()
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            DateTime Fdate = DateTime.Today.AddMonths(-1);
            DateTime Tdate = new DateTime();
            Fdate = Fdate.Date;
            Tdate = Tdate.Date;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM JP_TBL_Job";
                //cmd.Parameters.AddWithValue("@FDATE", SqlDbType.DATE).Value = Fdate;
                //cmd.Parameters.AddWithValue("@TDATE", SqlDbType.DATE).Value = Tdate;
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();

                return dt;
            }
        }
        public static DataTable GetTotalUser()
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM JP_TBL_Users";
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();

                return dt;
            }
        }
        public static DataTable GetTotalCompany()
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM JP_TBL_Company";
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();

                return dt;
            }
        }
        public static DataTable GetTotalAllJob()
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM JP_TBL_Job";
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();

                return dt;
            }
        }

        
    }
}
