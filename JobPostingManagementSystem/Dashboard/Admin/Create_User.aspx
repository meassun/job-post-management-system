﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="Create_User.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Admin.Create_User" %>

<asp:Content ID="Register_User" runat="server" ContentPlaceHolderID="DashContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
    <script type="text/javascript">

    </script>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" id="AddUser_panel" runat="server">
                <div class="col-md-12">
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title text-left"><b>Registration</b></h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div  class="col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtFname" runat="server" class="form-control" placeholder="Full name..."></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtusername" runat="server" class="form-control" placeholder="Username..."></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email..."></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control" placeholder="Password..."></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtConPassword" type="password" runat="server" class="form-control" placeholder="Confirm Password..." ></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:DropDownList ID="RoleList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceRoles" DataTextField="Rolename" DataValueField="RoleID">
                                            <asp:ListItem Selected="True" Value="0">--All Role--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSourceRoles" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Roles ORDER BY RoleID ASC"></asp:SqlDataSource>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:fileupload ID="FileUpload" runat="server" xmlns:asp="#unknown" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-6"></div>

                                <div class="col-md-4 col-md-offset-2">
                                    <asp:Button ID="btnSave" runat="server" class="btn btn-primary btn-block pull-right" Style="border: 1pt groove #d5d5d5; Width: 50%" Text="Save" ValidationGroup="Save" ToolTip="Add User" OnClick="btnSave_Click" />
                                </div>
                                <div class="col-md-4">
                                    <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-block pull-left" Style="border: 1pt groove #d5d5d5; Width: 50%" Text="Cancel" ValidationGroup="Cancel" ToolTip="Cancel" OnClick="btnCancel_Click" />
                                </div>

                            </fieldset>
                        </div>
                        <div class="login-help" style="text-align: center">
                            <asp:Label ID="lblResult" runat="server" Text="" Style="text-align: center;" ForeColor="Red"></asp:Label>
                        </div>
                        <br />
                    </div>
                </div>
            </div>

            <%--Success Registered Panel--%>
            <div class="row" id="Success_Create_panel" runat="server">
                <br />
                <br />
                <br />
                <br />
                <div class="col-md-12 ">
                    <div class="regist-panel  panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center text-uppercase" style="font-weight: bold;">Registeration</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <p>
                                            Dear
                                            <asp:Label ID="lblCreatedBy" runat="server" Text="" Style="text-align: center; font-weight: bold"></asp:Label><br>
                                            <br>
                                            New User: <asp:Label ID="lblUsername" runat="server" Style="text-align: center; font-weight: bold"/> have been registered.
                                        </p>
                                        <p>An e-mail has been sent to remind you of your login and password.</p>
                                        <br>
                                        <br>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
