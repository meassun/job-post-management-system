﻿using JobPostingManagementSystem.App_Code.DA.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard.Admin
{
    public partial class Create_User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Success_Create_panel.Attributes.CssStyle.Add("display", "none");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string fullName, userName, email, password, conpassword, Image, Role;
            string save_path = "~/Images/";
            fullName = txtFname.Text.Trim();
            userName = txtusername.Text.Trim();
            email = txtEmail.Text.Trim();
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);

            password = txtPassword.Text.Trim();
            conpassword = txtConPassword.Text.Trim();
            Image = Path.GetFileName(FileUpload.PostedFile.FileName);
            if (Image != "")
                FileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            Role = RoleList.SelectedValue.ToString(); //Admin user create--

            if (userName != "" && email != "" && password != "" && conpassword != "")
            {
                if (match.Success)
                {
                    if (password == conpassword)
                    {
                        //Check existing user registered
                        if (da_users.GetUserExistingRegister(userName, email) == false)
                        {
                            bool status = false;
                            status = da_users.AddNewUser(fullName, userName, email, password, Image, Role);
                            if (status == true)
                            {
                                AddUser_panel.Attributes.CssStyle.Add("display", "none");
                                Success_Create_panel.Attributes.CssStyle.Add("display", "block");
                                lblUsername.Text = userName;
                                lblCreatedBy.Text = Session["Username"].ToString();
                                clearSession(fullName, userName, email, password, Image, Role);
                            }
                            else
                            {
                                clearSession(fullName, userName, email, password, Image, Role);
                                lblResult.Text = "Add User failed!";
                            }
                        }
                        else
                        {
                            lblResult.Text = "Username is already exist!";
                        }
                    }
                    else
                    {
                        lblResult.Text = "Password is not match!";
                    }
                }
                else
                {
                    lblResult.Text = email + " is Invalid Email Address";
                }
                
            }
            else
            {
                clearSession(fullName, userName, email, password, Image, Role);
                lblResult.Text = "Please check username and password again!";
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        void clearSession(string fullName, string userName, string email, string pass, string Image, string role)
        {
            fullName = "";
            userName = "";
            email = "";
            pass = "";
            Image = "";
            role = "";
        }

    }
}