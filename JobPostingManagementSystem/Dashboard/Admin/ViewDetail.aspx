﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="ViewDetail.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Admin.ViewDetail" %>

<asp:Content ID="View_Pro" runat="server" ContentPlaceHolderID="DashContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
    <script type="text/javascript">

    </script>
    <div class="container">
        <%  
            try
            {
                if (lblUname.Text != "")
                {
        %>
        <div class="row" id="Edit_panel" runat="server">
            <div class="col-md-8 col-md-offset-2">
                <br />
                <br />
                <br />
                <br />
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-left"><b>Account Settings</b></h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <asp:Image ID="AccountImage" class="img-circle" runat="server" Style="width: 70px; height: auto" />
                                <b>
                                    <asp:Label ID="lblUname" runat="server" Text="" /></b><br />
                                <i class="fa fa-circle" style="color: lightgreen;" aria-hidden="true"></i>
                                <a href="#">
                                    <asp:Label ID="lblActive" runat="server" Text="Active" /></a>
                            </div>
                            <hr />
                            <div class="col-md-2">
                                <div class="form-group">
                                    <p><b>User name : </b></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtUsrname" class="form-control" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-6"></div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <p><b>Email : </b></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtEmail" class="form-control" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-6"></div>

                            <div class="col-md-4">
                                <asp:Button ID="btnSave" runat="server" class="btn btn-primary btn-block pull-right" Style="border: 1pt groove #d5d5d5; Width: 50%" Text="Save" ValidationGroup="Save" ToolTip="Add User" OnClick="btnSave_Click" />
                            </div>
                            <div class="col-md-4">
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-block pull-left" Style="border: 1pt groove #d5d5d5; Width: 50%" Text="Cancel" ValidationGroup="Cancel" ToolTip="Cancel" />
                            </div>

                        </fieldset>
                    </div>

                    <div class="ErrorLogin" style="text-align: center">
                        <asp:Label ID="lblResult" runat="server" Text="" Style="text-align: center;" ForeColor="Red"></asp:Label>
                    </div>
                    <br />
                </div>
            </div>
        </div>
        <%  
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            catch (Exception)
            {
                Response.Redirect("~/Login.aspx");
            }
                
        %>
    </div>

</asp:Content>
