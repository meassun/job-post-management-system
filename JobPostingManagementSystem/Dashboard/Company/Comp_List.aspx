﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="Comp_List.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Company.Comp_List" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="DashContent">
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
        });

        function ShowPopup() {
            $("#btnShowPopup").click();
        }

        function showAlert(errorStatus) {
            var message = "";
            $("#alert").show();
            if (errorStatus == "A") {
                message = "Add new company failed, Please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "B")
            {
                message = "Please check the information again!";
                $("#alert").show();
                $("#alert").addClass("alert-warning");
                $("p").text(message);
            }
            else if (errorStatus == "C") {
                message = "Update company failed, please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "D") {
                message = "Delete company failed, please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "E") {
                message = "Cannot find company to delete, please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "F") {
                message = "Cannot find company, please try again!";
                $("#alert").addClass("alert-warning");
                $("p").text(message);
            }
            else
            {
                message = "New company has been added successfully.";
                $("#alert").addClass("alert-success");
                $("p").text(message);
            }
            
            window.setTimeout(function () {
                $("#alert").fadeOut(100, function () {
                    $("#alert").hide();
                });
            }, 4000);
        };

    </script>

    <div id="page-wrapper">
        <div class="container-fluid">
            <br />
            <br />
            <div class="row">
                <div class="col-lg-9">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="m-t-small">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addCompanyModal"><i class="fa fa-fw fa-plus"></i>ADD COMPANY</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="alert" id="alert" role="alert" hidden>
                    <p></p>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Company List
                        </div>
                        <div class="panel-body">

                            <asp:GridView ID="Gv_CompanyList" runat="server" AutoGenerateColumns="False" DataKeyNames="CompanyID"
                                CssClass="footable" Style="max-width: 100%" AllowPaging="True" PageSize="10" AllowSorting="True" OnPageIndexChanging="Gv_CompanyList_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LOGO">
                                        <ItemTemplate>
                                            <asp:Image ID="Image" runat="server" ImageUrl='<%# "../../Images/Company/" + Eval("Logo")%>' Height="55px" Width="60px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CompanyName" HeaderText="NAME" SortExpression="CompanyName" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Province" HeaderText="LOCATION" SortExpression="CompanyName" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Email" HeaderText="EMAIL" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="PhoneNumber" HeaderText="PHONE" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="WebSite" HeaderText="SITE" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Underline="true" />
                                    <asp:BoundField DataField="Descript" HeaderText="DESCRIPTION" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="ACTION">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" class="form-control btn btn-xs btn-default btn-block " runat="server" CausesValidation="false" CommandName="Edits"
                                                CommandArgument='<%# Eval("CompanyID") %>' OnClick="GetCompanyInfo_Click" style="height:auto"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>

                                            <asp:LinkButton ID="Del" class="form-control btn btn-xs btn-danger btn-block " runat="server" CausesValidation="false" CommandName="Dels"
                                                CommandArgument='<%# Eval("CompanyID") %>' OnClick="btnDelete_Click" style="height:auto"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <button type="button" style="display: none;" id="btnShowPopup" class="btn btn-primary btn-lg"
                                data-toggle="modal" data-target="#EdtCompanyModal">
                            </button>
                            <asp:ScriptManager runat="server"></asp:ScriptManager>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- The modal -->
    <div class="modal fade" id="addCompanyModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Add Company</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtCname" runat="server" class="form-control" placeholder="Company name..."></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ProList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourcePro" DataTextField="Province_Name" DataValueField="Province_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Province--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourcePro" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Province ORDER BY Province_Name ASC"></asp:SqlDataSource>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email..."></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox type="number" ID="txtPhonenumber" runat="server" class="form-control" placeholder="Phone number..."></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtSite" runat="server" class="form-control" placeholder="www.example.com"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:fileupload id="FileUpload" runat="server" class="form-control" placeholder="Company profile" xmlns:asp="#unknown" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtAddress" class="form-control" TextMode="MultiLine" Columns="20" Rows="3" runat="server" placeholder="Company Address..." />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtDescript" class="form-control" TextMode="MultiLine" Columns="20" Rows="3" runat="server" placeholder="Company Description..." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Save" OnClick="btnSave_Click" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <%--The edit company modal--%>
    <div class="modal fade" id="EdtCompanyModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Edit Company</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <fieldset>
                            <div class="col-md-12 col-sm-6 col-xs-6" style="margin-bottom: 10px;">
                                <div class="form-group">
                                    <asp:Label ID="CompID" runat="server" Visible="false" />
                                    <div class="col-md-2 col-sm-2 col-xs-12" style="padding: 10px 5px; border: 1px solid #ddd; background: #fff; border-radius: 4px;">
                                        <asp:Image ID="CompanyImage" runat="server" Style="width: 100%; height: auto;" />
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 5%;">
                                        <b>
                                            <asp:Label ID="ComName" class="text-primary text-center" runat="server" Style="font-size: large;"></asp:Label></b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"></div>
                            <br />
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtCompanyName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:DropDownList ID="edtProList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourcePro" DataTextField="Province_Name" DataValueField="Province_ID">
                                        <asp:ListItem Selected="True" Value="0">--All Province--</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Province ORDER BY Province_Name ASC"></asp:SqlDataSource>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox type="number" ID="edtNumber" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtWebsite" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:fileupload id="edtFileUpload" runat="server" class="form-control" xmlns:asp="#unknown" />
                                    <asp:TextBox ID="edtImage" runat="server" class="form-control" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtAddress" class="form-control" TextMode="MultiLine" Columns="20" Rows="3" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtDesc" class="form-control" TextMode="MultiLine" Columns="20" Rows="3" runat="server" />
                                </div>
                            </div>
                        </fieldset>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnEdit" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Edit" OnClick="btnEdit_Click" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
