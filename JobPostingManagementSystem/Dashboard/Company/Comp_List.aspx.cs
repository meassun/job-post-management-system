﻿using JobPostingManagementSystem.App_Code.DA.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard.Company
{
    public partial class Comp_List : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void LoadData() 
        {
            Gv_CompanyList.DataSource = da_company.GetCompanyList();
            Gv_CompanyList.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string comName, province, email, mphone, webSite, Image, address, descript;
            comName = txtCname.Text.Trim();
            province = ProList.SelectedValue.ToString();
            email = txtEmail.Text.Trim();
            mphone = txtPhonenumber.Text.Trim();
            webSite = txtSite.Text.Trim();
            address = txtAddress.Text.Trim();
            descript = txtDescript.Text.Trim();
            string save_path = "~/Images/Company/";
            Image = Path.GetFileName(FileUpload.PostedFile.FileName);
            if (Image != "")
            {
                FileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            }
            if (comName != "" && province != "" && email != "")
            {
                bool status = da_company.AddNewCompany(comName, province, email, mphone, webSite, Image,address, descript);
                if (status != false)
                {
                    Response.Redirect("~/Dashboard/Company/Comp_List.aspx");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "A" + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "B" + "');", true);
            }
        }

        protected void GetCompanyInfo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton ComID = sender as LinkButton;
                if (ComID.CommandArgument != "")
                {
                    int companyID = Int32.Parse(ComID.CommandArgument); //get the company id from user click 

                    DataTable dt = da_company.GetCompanyInfoByEdit(companyID);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            CompID.Text = item["Company_ID"].ToString();
                            edtCompanyName.Text = item["Company_Name"].ToString();
                            edtProList.SelectedValue = item["Province_ID"].ToString();
                            edtEmail.Text = item["Email"].ToString();
                            edtNumber.Text = item["Phone_number"].ToString();
                            edtWebsite.Text = item["Website"].ToString();
                            edtAddress.Text = item["Located"].ToString();
                            edtDesc.Text = item["Descript"].ToString();
                            CompanyImage.ImageUrl = "~/Images/Company/" + item["Logo"].ToString();
                            edtImage.Text = item["Logo"].ToString();
                            ComName.Text = "&nbsp;&nbsp;&nbsp;" + item["Company_Name"].ToString();
                            break;
                        }
                        
                    }
                    
                    //Show Modal Edit
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "ShowPopup();", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "F" + "');", true);
                }
            }
            catch (Exception)
            {

            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int comID = 0;
            string companyID, comName, province, email, mphone, webSite, Image, address, descript;
            companyID = CompID.Text.Trim();
            comID = Int32.Parse(companyID);
            comName = edtCompanyName.Text.Trim();
            province = edtProList.SelectedValue.ToString();
            email = edtEmail.Text.Trim();
            mphone = edtNumber.Text.Trim();
            webSite = edtWebsite.Text.Trim();
            address = edtAddress.Text.Trim();
            descript = edtDesc.Text.Trim();
            string save_path = "~/Images/Company/";
            Image = Path.GetFileName(edtFileUpload.PostedFile.FileName);
            if (Image != "")
                edtFileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            else
                Image = edtImage.Text;
            if (comName != "" && province != "" && email != "")
            {
                bool status = da_company.EditCompany(comID, comName, province, email, mphone, webSite, Image, address, descript);
                if (status != false)
                {
                    Response.Redirect("~/Dashboard/Company/Comp_List.aspx");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "C" + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "B" + "');", true);
            }

                
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton ComID = sender as LinkButton;
                if (ComID.CommandArgument != "")
                {
                    int CompanyID = Int32.Parse(ComID.CommandArgument);
                    if (CompanyID > 0)
                    {
                        bool status = da_company.DeleteCompany(CompanyID);
                        if (status != false)
                        {

                            Response.Redirect("~/Dashboard/Company/Comp_List.aspx");
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "D" + "');", true);
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "E" + "');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "F" + "');", true);
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        protected void Gv_CompanyList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Gv_CompanyList.PageIndex = e.NewPageIndex;
            LoadData();
        }


    }
}