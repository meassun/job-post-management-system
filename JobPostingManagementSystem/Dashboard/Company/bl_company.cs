﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobPostingManagementSystem.Dashboard.Company
{
    public class bl_company
    {
        public bl_company()
        {

        }

        #region //Properties bl_company
        public int Job_ID { get; set; }
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Location { get; set; } 
        public int ProvinceID { get; set; }
        public string Province { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Logo { get; set; }
        public string WebSite { get; set; }
        public string Descript { get; set; }
        public DateTime CreatedOn { get; set; }
        
        #endregion

        #region //Properties active job By company
        public string Position { get; set; }
        public string Category { get; set; }
        public string Salary { get; set; }
        public string ExpireDate { get; set; }
        #endregion


    }
}
