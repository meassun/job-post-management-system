﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace JobPostingManagementSystem.Dashboard.Company
{
    class da_company
    {
        public da_company()
        {

        }

        public static List<bl_company> GetCompanyList()
        {
            List<bl_company> comList = new List<bl_company>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Company C INNER JOIN JP_TBL_Company_Contact CC ON C.Company_ID = CC.Company_ID " +
                        "INNER JOIN JP_TBL_Province P ON C.Province_ID = P.Province_ID";
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            comList.Add(new bl_company
                            {
                                CompanyID = Convert.ToInt32(item["Company_ID"].ToString()),
                                CompanyName = item["Company_Name"].ToString(),
                                Province = item["Province_Name"].ToString(),
                                Location = item["Located"].ToString(),
                                Email = item["Email"].ToString(),
                                PhoneNumber = item["Phone_number"].ToString(),
                                Logo = item["Logo"].ToString(),
                                WebSite = item["Website"].ToString(),
                                Descript = item["Descript"].ToString(),
                                CreatedOn = Convert.ToDateTime(item["Created_On"].ToString())
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return comList;
        }

        public static bool AddNewCompany(string cname, string province, string email, string phone, string website, string image, string address, string desc)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_ADD_COMPANY";
                    cmd.Parameters.AddWithValue("@ComName", cname);
                    cmd.Parameters.AddWithValue("@Provice", province);
                    cmd.Parameters.AddWithValue("@Address", address);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@Phone", phone);
                    cmd.Parameters.AddWithValue("@website", website);
                    cmd.Parameters.AddWithValue("@image", image);
                    cmd.Parameters.AddWithValue("@desc", desc);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }
        }

        public static DataTable GetCompanyInfoByEdit(int company_id)
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "JP_SP_GET_COMPANY_BY_EDIT";
                cmd.Parameters.AddWithValue("@companyID", company_id);
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();
                return dt;
            }
        }

        public static bool EditCompany(int ComID, string cname, string province, string email, string phone, string wesbiste, string image, string address, string descs)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_EDIT_COMPANY";
                    cmd.Parameters.AddWithValue("@CompanyID", ComID);
                    cmd.Parameters.AddWithValue("@ComName", cname);
                    cmd.Parameters.AddWithValue("@Provice", province);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@Phone", phone);
                    cmd.Parameters.AddWithValue("@website", wesbiste);
                    cmd.Parameters.AddWithValue("@image", image);
                    cmd.Parameters.AddWithValue("@Address", address);
                    cmd.Parameters.AddWithValue("@desc", descs);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }

        }

        public static bool DeleteCompany(int com_id)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_DELETE_COMPANY";
                    cmd.Parameters.AddWithValue("@CompanyID", com_id);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }

        }

        public static List<bl_company> GetActiveJobByCompany(string company_id)
        {
            List<bl_company> activeJobByCompany = new List<bl_company>();
            DataTable dt = new DataTable();
            SqlDataAdapter mydata;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Company C INNER JOIN JP_TBL_Job J ON C.Company_ID = J.Company_ID " 
                                       + "INNER JOIN JP_TBL_Province P ON P.Province_ID = C.Province_ID " 
                                       + "INNER JOIN JP_TBL_Company_Contact CC ON CC.Company_ID = C.Company_ID "
                                       + "INNER JOIN JP_TBL_Position PS  ON PS.Position_ID = J.Position_ID "
                                       + "INNER JOIN JP_TBL_Category CA  ON CA.Category_ID = PS.Category_ID "
                                       + "INNER JOIN JP_TBL_Salary SA  ON SA.Salary_ID = J.Salary_ID WHERE C.Company_ID = @CompanyID";
                    cmd.Parameters.AddWithValue("@CompanyID", company_id);
                    cmd.Connection = con;
                    con.Open(); 
                    mydata = new SqlDataAdapter(cmd);
                    mydata.Fill(dt);
                    mydata.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            activeJobByCompany.Add(new bl_company()
                            {
                                CompanyID = Convert.ToInt32(item["Company_ID"].ToString()),
                                CompanyName = item["Company_Name"].ToString(),
                                Province = item["Province_Name"].ToString(),
                                Location = item["Located"].ToString(),
                                Email = item["Email"].ToString(),
                                PhoneNumber = item["Phone_number"].ToString(),
                                Logo = item["Logo"].ToString(),
                                WebSite = item["Website"].ToString(),
                                Descript = item["Descript"].ToString(),
                                CreatedOn = Convert.ToDateTime(item["Created_On"].ToString()),
                                Job_ID = Convert.ToInt32(item["ID"].ToString()),
                                Position = item["Position_Name"].ToString(),
                                Category = item["Category_Name"].ToString(),
                                Salary = item["Salary"].ToString(),
                                ExpireDate = item["Expired_Date"].ToString()

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return activeJobByCompany;
        }
        
    }
}
