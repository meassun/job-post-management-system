﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard
{
    public partial class Dash : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                lblUsername.Text = Session["Username"].ToString();
                AccountImage.ImageUrl = "~/Images/" + Session["Profiles"].ToString();
                
            }
            catch (Exception)
            {
                Response.Redirect("~/Default.aspx");
            }
            
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("~/Default.aspx");
        }
    }
}