﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="Default.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Default" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="DashContent">
    <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <h2 class="page-header text-bold text-primary" style="font-family:Algerian">Welcome To Dashboard</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-bullhorn fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">
                                                <asp:Label ID="lblNewJob" Text="" runat="server" />
                                            </div>
                                            <div>New Job Post</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="../../Dashboard/Jobs/Default.aspx">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-users fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">
                                                <asp:Label ID="lblUser" Text="" runat="server" />
                                            </div>
                                            <div>Users</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="../../Dashboard/Users/Manage_Users.aspx">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-university fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">
                                                <asp:Label ID="lblTotalCompany" Text="" runat="server" />
                                            </div>
                                            <div>All Company</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="../../Dashboard/Company/Comp_List.aspx">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-briefcase fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">
                                                <asp:Label ID="lblTotalJob" Text="" runat="server" />
                                            </div>
                                            <div>All Jobs</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="../../Dashboard/Jobs/Default.aspx">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Jobs By Category
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-4">

                                        </div>
                                        <div class="col-lg-8">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
</asp:Content>


