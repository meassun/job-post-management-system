﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobPostingManagementSystem.App_Code;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using JobPostingManagementSystem.App_Code.DA.Users;


namespace JobPostingManagementSystem.Dashboard
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable TotalCurrentPostJob = new DataTable();
            DataTable TotalUsers = new DataTable();
            DataTable TotalCompany = new DataTable();
            DataTable TotalJob = new DataTable();

            TotalCurrentPostJob = da_users.GetTotalCurrentPostJob();
            TotalUsers = da_users.GetTotalUser();
            TotalCompany = da_users.GetTotalCompany();
            TotalJob = da_users.GetTotalAllJob();

            if (TotalCurrentPostJob.Rows.Count > 0)
                lblNewJob.Text = (TotalCurrentPostJob.Rows.Count).ToString();
            else
                lblNewJob.Text = "0";
            if (TotalUsers.Rows.Count > 0)
                lblUser.Text = (TotalUsers.Rows.Count).ToString();
            else
                lblUser.Text = "0";
            if (TotalCompany.Rows.Count > 0)
                lblTotalCompany.Text = (TotalCompany.Rows.Count).ToString();
            else
                lblTotalCompany.Text = "0";
            if (TotalJob.Rows.Count > 0)
                lblTotalJob.Text = (TotalJob.Rows.Count).ToString();
            else
                lblTotalJob.Text = "0";
        }


    }
}