﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="Default.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Jobs.Default" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="DashContent">
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            getAllJobList();
        });
       
        var JobID = 0;
        function getAllJobList()
        {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetJobList",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var jobList = response.d;
                    var count = 0;
                    $.each(jobList, function (index, job) {
                        count += 1;
                        $("#view-job tbody").append('<tr id= '+job.ID+'><td>' + count + '</td><td>'
                            + job.CreatedOn + '</td><td>' + job.CompanyName + '</td><td>'
                            + job.Position + '</td><td>' + job.JobType + '</td><td><strong>'
                            + job.Priority + '</strong></td><td>' + job.Schedule + '</td><td>'
                            + job.Province + '</td><td>' + job.Salary + '</td><td>'
                            + job.ExpiredOn + '</td><td class="text-center"><button class="btn btn-xs btn-primary" type="button" onclick="EditJob(' + job.ID + ')"><i class="fa fa-pencil-square-o text-default text-center"></i> Edit</button>'
                            + '&nbsp;&nbsp;<button class="btn btn-xs btn-danger" type="button" onClick="DeleteJob(' + job.ID + ')"><i class="fa fa-trash text-default text-center"></i> Del</button></td></tr>');
                    });
                },
                error: function () {

                }
            });
        }
        function DeleteJob(ID) {
            var r = confirm("Are you sure want to delete?");
            if (r == true) {
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "Default.aspx/DeleteJob",
                    data: '{job_id:' + ID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (status) {
                        if (status.d == 1) {
                            $("#view-job tr#" + ID).remove();
                            showAlert("Job has been deleted.", "alert-success");
                        } else {
                            showAlert("Delete job failed!", "alert-danger");
                        }

                    }
                });
            }
        }

        function EditJob(ID) {
            $.ajax({
                async: true,
                type: "POST",
                url: 'Default.aspx/GetUpdateJob?job_id=' + ID,
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var JobList = response.d;
                    $.each(JobList, function (index, job) {
                        JobID = job.ID;
                        $('#<%=ddlCompanyName.ClientID%>').val(job.CompanyName);
                        $('#<%=ddlPosList.ClientID%>').val(job.Position);
                        $('#<%=ddlSalaryList.ClientID%>').val(job.Salary);
                        $('#<%=ddlJobType.ClientID%>').val(job.JobType);
                        $('#<%=ddlPrioList.ClientID%>').val(job.Priority);
                        $('#<%=txtEdtSchedule.ClientID%>').val(job.Schedule);
                        $('#<%=txtEdtExpireDate.ClientID%>').val(job.ExpiredOn);
                    });
                    ShowPopup();
                },
                error: function () {
                }
            });
        }
        function OnUpdate() {
            var ID = JobID;
            var CompanyName = $('#<%=ddlCompanyName.ClientID%>').val();
            var Position = $('#<%=ddlPosList.ClientID%>').val();
            var Salary = $('#<%=ddlSalaryList.ClientID%>').val();
            var JobType = $('#<%=ddlJobType.ClientID%>').val();
            var Priority = $('#<%=ddlPrioList.ClientID%>').val();
            var Schedule = $('#<%=txtEdtSchedule.ClientID%>').val();
            var ExpiredOn = $('#<%=txtEdtExpireDate.ClientID%>').val();
            var JobList = { id: ID, CompanyName: CompanyName, Position: Position, 
                Salary: Salary, JobType: JobType, Priority: Priority, Schedule: Schedule, ExpiredOn: ExpiredOn };
            var data = JSON.stringify(JobList);
            $.ajax({
                url: 'Default.aspx/SaveEdit',
                type: 'POST',
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (status) {
                    if (status.d != 0) {
                        $("#view-job tbody").empty();
                        getAllJobList();
                        showAlert("Job has been updated.", "alert-success");
                    }
                    else {
                        showAlert("Updating failed!", "alert-danger");
                    }
                    
                },
                error: function (err) {
                    console.log(err);
                }
            });
        };

        function showAlert(message, alert) {
            $("#alert").show();
            $("#alert").addClass(alert);
            $("p").text(message);
            window.setTimeout(function () {
                $("#alert").fadeOut(100, function () {
                    $("#alert").hide();
                });
            }, 6000);
        };

        function ShowPopup() {
            $("#btnShowPopup").click();
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>
    <div id="page-wrapper">
        <div class="container-fluid">
            <br />
            <br />
            <div class="row">
                <div class="col-lg-9">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="m-t-small">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#postJobModal"><i class="fa fa-fw fa-plus"></i>POST JOB</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="alert" id="alert" role="alert" hidden>
                    <p></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12"> 

                    <section class="content">
                        <div class="box box-info">
                            <div class="row" style="margin-right: 0.2%; margin-top: 0%;">
                                <div class="col-sm-12 table-responsive">
                                    <div id="view-job_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                        <div class="pull-right">
                                            <div id="view-job_filter" class="dataTables_filter">
                                                <label><input type="search" class="form-control input-sm" placeholder="Search Job" aria-controls="view-loans"></label>
                                            </div>
                                        </div>
                                        <table id="view-job" class="table table-condensed table-hover dataTable" style="width: 100%; border: 1pt groove #ffffff;" role="grid">
                                            <thead>
                                                <tr style="background-color: #D1F9FF" role="row">
                                                    <th style="width: 60px;">No</th>
                                                    <th class="sorting" style="width: 82px;">Released</th>
                                                    <th class="sorting" style="width: 122px;">Company</th>
                                                    <th style="width: 58px;">Position</th>
                                                    <th style="width: 82px;">Type</th>
                                                    <th style="width: 56px;">Status</th>
                                                    <th style="width: 45px;">Working</th>
                                                    <th style="width: 74px;">Location</th>
                                                    <th style="width: 121px;">Salary</th>
                                                    <th style="width: 63px;">Expired</th>
                                                    <th style="width: 60px;">Action</th>
                                                </tr>
                                            </thead>

                                            <tfoot class="bg-gray">
                                            </tfoot>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                        <div class="dataTables_info" id="view-job_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
                                        <div class="dataTables_paginate paging_simple_numbers" id="view-job_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button previous disabled" id="view-job_previous"><a href="#" aria-controls="view-job" data-dt-idx="0" tabindex="0">Previous</a></li>
                                                <li class="paginate_button active"><a href="#" aria-controls="view-job" data-dt-idx="1" tabindex="0">1</a></li>
                                                <li class="paginate_button next disabled" id="view-job_next"><a href="#" aria-controls="view-job" data-dt-idx="2" tabindex="0">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>

                    <button type="button" style="display: none;" id="btnShowPopup" class="btn btn-primary btn-lg"
                        data-toggle="modal" data-target="#EdtJobModal">
                    </button>
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                </div>
            </div>
        </div>
    </div>
    <!-- The modal -->
    <div class="modal fade" id="postJobModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Post New Job</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <fieldset>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Company Name</label>
                                <asp:DropDownList ID="CompanyList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceCompany" DataTextField="Company_Name" DataValueField="Company_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Company--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceCompany" runat="server" ConnectionString="<% $ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Company ORDER BY Company_Name ASC"></asp:SqlDataSource>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Position</label>
                                <asp:DropDownList ID="DrPosition" runat="server" class="form-control" AppendDataBoundItems="True" DataSourceID="SqlDataSourcePosition" DataTextField="Position_Name" DataValueField="Position_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Fields--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourcePosition" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Position ORDER BY Position_Name ASC "></asp:SqlDataSource>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Salary</label>
                                <asp:DropDownList ID="SalaryList" runat="server" class="form-control" AppendDataBoundItems="True" DataSourceID="SqlDataSourceSalary" DataTextField="Salary" DataValueField="Salary_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Salary--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceSalary" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Salary ORDER BY Salary_ID ASC "></asp:SqlDataSource>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Job Type</label>
                                <asp:DropDownList ID="JobTypeList" runat="server"  class="form-control" AppendDataBoundItems="True" DataSourceID="SqlDataSourceJobType" DataTextField="Job_Type" DataValueField="Job_Type_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Type--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceJobType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Job_Type ORDER BY Job_Type_ID ASC "></asp:SqlDataSource>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Priority</label>
                                <asp:DropDownList ID="PriorityList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourcePriority" DataTextField="Status" DataValueField="Prio_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Priority--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourcePriority" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Priority ORDER BY Prio_ID ASC "></asp:SqlDataSource>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Schedule</label>
                                <div class='input-group' >
                                    <asp:TextBox ID="txtSchedule" runat="server" class="form-control  datetimepicker" TabIndex="1" placeholder="mm-dd-yyyy"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Expiration Date</label>
                                <div class='input-group' >
                                    <asp:TextBox type="date" ID="txtExpireDate" runat="server" class="form-control  datetimepicker" TabIndex="1" placeholder="mm-dd-yyyy"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Save" OnClick="btnSubmit_Click" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <%--The Edit Modal--%>
    <div class="modal fade" id="EdtJobModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Edit Job</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <fieldset>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlCompanyName" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceEdtCompanyName" DataTextField="Company_Name" DataValueField="Company_ID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceEdtCompanyName" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Company ORDER BY Company_Name ASC"></asp:SqlDataSource>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlPosList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceEdtPos" DataTextField="Position_Name" DataValueField="Position_ID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceEdtPos" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Position ORDER BY Position_Name ASC"></asp:SqlDataSource>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlSalaryList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceEdtSalary" DataTextField="Salary" DataValueField="Salary_ID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceEdtSalary" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Salary ORDER BY Salary_ID ASC "></asp:SqlDataSource>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlJobType" runat="server" class="form-control" AppendDataBoundItems="True" DataSourceID="SqlDataSourceEdtJobType" DataTextField="Job_Type" DataValueField="Job_Type_ID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceEdtJobType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Job_Type ORDER BY Job_Type_ID ASC "></asp:SqlDataSource>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlPrioList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceEdtPrio" DataTextField="Status" DataValueField="Prio_ID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceEdtPrio" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Priority ORDER BY Prio_ID ASC "></asp:SqlDataSource>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox type="date" ID="txtEdtSchedule" runat="server" class="form-control" TabIndex="1" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox type="date" ID="txtEdtExpireDate" runat="server" class="form-control" TabIndex="1" ></asp:TextBox>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="panel-footer">
                    <button id="btn_update" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" style="border: 1pt groove #d5d5d5;" type="button" onClick="OnUpdate()">Edit</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
