﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard.Jobs
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<bl_job>GetJobList()
        {
            List<bl_job> jobList = new List<bl_job>();
            jobList = da_job.GetJobList();

            return jobList;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string CreatedBy, UpdatedBy, JobType, Company, Position, Salary, Priority, ExpireDate;
                DateTime CreatedOn = DateTime.Now;
                DateTime Schedule = DateTime.Now;

                CreatedBy = Session["Username"].ToString();
                UpdatedBy = Session["Username"].ToString();
                Company = CompanyList.SelectedValue.ToString();
                //Location = DrLocation.SelectedValue.ToString();
                Position = DrPosition.SelectedValue.ToString();
                Salary = SalaryList.SelectedValue.ToString();
                if (int.Parse(Salary) == 0)
                {
                    Salary = null;
                }
                JobType = JobTypeList.SelectedValue.ToString();
                Priority = PriorityList.SelectedValue.ToString();
                CreatedOn.ToString("MM/dd/yyyy");
                ExpireDate = txtExpireDate.Text.Trim();
                if (Company != "" && Position != "" && Company != "" && ExpireDate != "")
                {
                    bool status = da_job.AddNewJob(CreatedOn.ToString("MM/dd/yyyy"), CreatedBy, UpdatedBy, JobType, Schedule.ToString("MM/dd/yyyy"), Priority, Company, Position, Salary, ExpireDate);
                    if (status != false)
                    {
                        Response.Redirect("~/Dashboard/Jobs/Default.aspx");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Add job is failed!, please try again!" + "'" + "," + " '" + "alert-danger" + "');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Adding is failed, please check information again!" + "'" + "," + " '" + "alert-warning" + "');", true);
                }

            }
            catch (Exception)
            {
            }

        }

        [WebMethod]
        public static int DeleteJob(string job_id)
        {
            int status = 0;
            status = da_job.DeleteJob(job_id);
            if (status != 0)
                return 1;
            else
                return status;
        }

        [WebMethod]
        public static List<bl_job> GetUpdateJob()
        {
            string job_id = HttpContext.Current.Request.QueryString["job_id"]; 
            var List = new bl_job();
            List<bl_job> JobList = new List<bl_job>();
            try
            {
                DataTable dt = da_job.GetJobInfoByEdit(job_id);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow job in dt.Rows)
                        {
                            JobList.Add(new bl_job
                            {
                                ID = Convert.ToInt32(job["ID"]).ToString(),
                                LOGO = job["LOGO"].ToString(),
                                CompanyName = job["Company_ID"].ToString(),
                                Position = job["Position_ID"].ToString(),
                                Category = job["Category_ID"].ToString(),
                                JobType = job["Job_Type_ID"].ToString(),
                                Priority = job["Prio_ID"].ToString(),
                                Schedule = job["Schedule"].ToString(),
                                Province = job["Province_ID"].ToString(),
                                Salary = job["Salary_ID"].ToString(),
                                CreatedBy = job["Created_By"].ToString(),
                                CreatedOn = job["Created_On"].ToString(),
                                ExpiredOn = job["Expired_Date"].ToString()
                            });
                            
                        }
                    }
                    
            }
            catch (Exception)
            {

            }
            return JobList;
        }

        [WebMethod]
        public static int SaveEdit(string id, string CompanyName, string Position, string Salary, string JobType, string Priority, string Schedule, string ExpiredOn)
        {
            int status = 0;
            status = da_job.SaveEdit(id, CompanyName, Position, Salary, JobType, Priority, Schedule, ExpiredOn);
            if (status != 0)
                return 1;
            else
                return status;
                
        }

    }
}