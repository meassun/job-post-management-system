﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Jobs
{
    public class bl_job
    {
        public bl_job()
        { 
        
        }
        public string ID { get; set; }
        public string LOGO { get; set; }
        public string User { get; set; }
        public string Profiles { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string Category { get; set; }
        public string JobType { get; set; }
        public string Priority { get; set; }
        public string Schedule { get; set; }
        public string Province { get; set; }
        public string Salary { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ExpiredOn { get; set; }

        public int LikeAmount { get; set; }
        public int CandidateCV { get; set; }

    }
}