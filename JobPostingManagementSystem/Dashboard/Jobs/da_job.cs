﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Jobs
{
    public class da_job
    {
        public da_job()
        { 
        
        }

        public static List<bl_job> GetJobList()
        {
            List<bl_job> JobList = new List<bl_job>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_GET_JOB_LIST";
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow job in dt.Rows)
                        {
                            JobList.Add(new bl_job
                            {
                                ID = Convert.ToInt32(job["ID"]).ToString(),
                                LOGO = job["LOGO"].ToString(),
                                CompanyName = job["Company_Name"].ToString(),
                                Position = job["Position_Name"].ToString(),
                                Category = job["Category_Name"].ToString(),
                                JobType = job["Job_Type"].ToString(),
                                Priority = job["Status"].ToString(),
                                Schedule = job["Schedule"].ToString(),
                                Province = job["Province_Name"].ToString(),
                                Salary = job["Salary"].ToString(),
                                CreatedBy = job["Created_By"].ToString(),
                                CreatedOn = job["Created_On"].ToString(),
                                ExpiredOn = job["Expired_Date"].ToString()
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return JobList;
        }

        public static List<bl_job> GetLatestCompanyJobPostList()
        {
            List<bl_job> latestComList = new List<bl_job>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 6 * FROM JP_TBL_Job A INNER JOIN JP_TBL_Company_Contact B ON A.Company_ID = B.Company_ID INNER JOIN JP_TBL_Company C ON C.Company_ID = B.Company_ID ORDER BY A.Created_On DESC ";
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow job in dt.Rows)
                        {
                            latestComList.Add(new bl_job
                            {
                                ID = Convert.ToInt32(job["ID"]).ToString(),
                                CompanyID = Convert.ToInt32(job["Company_ID"]).ToString(),
                                LOGO = job["LOGO"].ToString(),
                                CompanyName = job["Company_Name"].ToString(),
                                CreatedOn = job["Created_On"].ToString()
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return latestComList;
        }

        public static bool AddNewJob(string created_on, string created_by, string updated_by, string job_type, string schedule, string Priority, string company, string position, string salary, string expired_date)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_POST_NEW_JOB";
                    cmd.Parameters.AddWithValue("@CreatedOn", created_on);
                    cmd.Parameters.AddWithValue("@CreatedBy", created_by);
                    cmd.Parameters.AddWithValue("@Company", company);
                    cmd.Parameters.AddWithValue("@UpdatedBy", updated_by);
                    cmd.Parameters.AddWithValue("@JobType", job_type);
                    cmd.Parameters.AddWithValue("@Schedule", schedule);
                    cmd.Parameters.AddWithValue("@Priority", Priority);
                    cmd.Parameters.AddWithValue("@Position", position);
                    cmd.Parameters.AddWithValue("@Salary", salary);
                    cmd.Parameters.AddWithValue("@ExpireDate", expired_date);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;

                }
            }
            catch (Exception)
            {
                return status;
            }

        }

        public static DataTable GetJobInfoByEdit(string id)
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "JP_SP_GET_JOB_BY_EDIT";
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();
                return dt;
            }


        }

        public static int DeleteJob(string id)
        {
            int status = 0;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlCommand cmd1 = new SqlCommand();
                    SqlCommand cmd2 = new SqlCommand();
                    SqlCommand cmd3 = new SqlCommand();

                    cmd.CommandType = CommandType.Text;
                    cmd1.CommandText = "DELETE FROM JP_TBL_Job_Like WHERE Job_ID = @ID";
                    cmd2.CommandText = "DELETE FROM JP_TBL_Candidate WHERE Job_ID = @ID";
                    cmd3.CommandText = "DELETE FROM JP_TBL_Job_Bookmark WHERE Job_ID = @ID";
                    cmd.CommandText = "DELETE FROM JP_TBL_Job WHERE ID = @ID";
                    cmd1.Connection = con;
                    cmd2.Connection = con;
                    cmd3.Connection = con;
                    cmd.Connection = con;
                    cmd1.Parameters.AddWithValue("@ID", id);
                    cmd2.Parameters.AddWithValue("@ID", id);
                    cmd3.Parameters.AddWithValue("@ID", id);
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.Connection = con;
                    con.Open();
                    cmd1.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    cmd3.ExecuteNonQuery();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return 1;

                }
            }
            catch (Exception)
            {
                return status;
            }
        }

        public static bool UpdateJob(string id)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_UPDATE_JOB";
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;

                }
            }
            catch (Exception)
            {
                return status;
            }
        }

        public static int SaveEdit(string id, string company_name, string position, string salary, string job_type, string priority, string schedule, string expire_date)
        {
            int status = 0;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_UPDATE_JOB";
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.Parameters.AddWithValue("@Name", company_name);
                    cmd.Parameters.AddWithValue("@Pos", position);
                    cmd.Parameters.AddWithValue("@Sal", salary);
                    cmd.Parameters.AddWithValue("@Type", job_type);
                    cmd.Parameters.AddWithValue("@Prio", priority);
                    cmd.Parameters.AddWithValue("@Sched", schedule);
                    cmd.Parameters.AddWithValue("@Expire", Convert.ToDateTime(expire_date));
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return 1;

                }
            }
            catch (Exception ex)
            {
                return status;
            }
        }

        public static List<bl_job> GetActiveJobByCategoryDetail(string category_id)
        {
            List<bl_job> activeJobByCategory = new List<bl_job>();
            DataTable dt = new DataTable();
            SqlDataAdapter mydata;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Job J INNER JOIN JP_TBL_Position PS ON PS.Position_ID = J.Position_ID "
                                       + "INNER JOIN JP_TBL_Company C ON C.Company_ID = J.Company_ID "
                                       + "INNER JOIN JP_TBL_Company_Contact CC ON CC.Company_ID = C.Company_ID "
                                       + "INNER JOIN JP_TBL_Province P ON P.Province_ID = C.Province_ID "
                                       + "INNER JOIN JP_TBL_Category CA  ON CA.Category_ID = PS.Category_ID "
                                       + "INNER JOIN JP_TBL_Job_Type JT  ON JT.Job_Type_ID = J.Job_Type_ID "
                                       + "INNER JOIN JP_TBL_Priority PR  ON PR.Prio_ID = J.Prio_ID "
                                       + "INNER JOIN JP_TBL_Salary SA  ON SA.Salary_ID = J.Salary_ID WHERE CA.Category_ID = @CategoryID";
                    cmd.Parameters.AddWithValue("@CategoryID", category_id);
                    cmd.Connection = con;
                    con.Open();
                    mydata = new SqlDataAdapter(cmd);
                    mydata.Fill(dt);
                    mydata.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            activeJobByCategory.Add(new bl_job()
                            {
                                ID = item["ID"].ToString(),
                                CompanyID = item["Company_ID"].ToString(),
                                CompanyName = item["Company_Name"].ToString(),
                                LOGO = item["Logo"].ToString(),
                                Position = item["Position_Name"].ToString(),
                                Category = item["Category_Name"].ToString(),
                                JobType = item["Job_Type"].ToString(),
                                Priority = item["Status"].ToString(),
                                Schedule = item["Schedule"].ToString(),
                                Province = item["Province_Name"].ToString(),
                                Salary = item["Salary"].ToString(),
                                CreatedOn = item["Created_On"].ToString(),
                                CreatedBy = item["Created_By"].ToString(),
                                ExpiredOn = item["Expired_Date"].ToString(),

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return activeJobByCategory;
        }

        public static List<bl_job> GetJobSearch(string category, string pos, string location, string salary)
        {
            List<bl_job> JobSearchList = new List<bl_job>();
            DataTable dt = new DataTable();
            SqlDataAdapter mydata;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    string command = "SELECT * FROM JP_TBL_Job J "
                                        + "INNER JOIN jp_tbl_Company C ON c.Company_ID = J.Company_ID "
                                        + "INNER JOIN JP_TBL_Company_Contact CC ON CC.Company_ID = C.Company_ID "
                                        + "INNER JOIN JP_TBL_Province p ON p.Province_ID = C.Province_ID "
                                        + "INNER JOIN JP_TBL_Position PS  ON J.Position_ID = PS.Position_ID "
                                        + "INNER JOIN JP_TBL_Category CA  ON CA.Category_ID = PS.Category_ID "
                                        + "INNER JOIN JP_TBL_Job_Type JT  ON JT.Job_Type_ID = J.Job_Type_ID "
                                        + "INNER JOIN JP_TBL_Priority PR  ON PR.Prio_ID = J.Prio_ID "
                                        + "INNER JOIN JP_TBL_Salary SA  ON SA.Salary_ID = J.Salary_ID ";
                    string sql = "";
                    if (int.Parse(category) != 0 && int.Parse(pos) != 0 && int.Parse(location) != 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND J.Position_ID = @Pos AND P.Province_ID = @Locat AND J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) != 0 && int.Parse(location) != 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE J.Position_ID = @Pos AND P.Province_ID = @Locat AND J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) == 0 && int.Parse(location) != 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND P.Province_ID = @Locat AND J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) != 0 && int.Parse(location) == 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND J.Position_ID = @Pos AND J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) != 0 && int.Parse(location) != 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND J.Position_ID = @Pos AND P.Province_ID = @Locat";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) == 0 && int.Parse(location) != 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE P.Province_ID = @Locat AND J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) == 0 && int.Parse(location) == 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) == 0 && int.Parse(location) != 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE P.Province_ID = @Locat";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) != 0 && int.Parse(location) == 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE J.Position_ID = @Pos";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) == 0 && int.Parse(location) == 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) == 0 && int.Parse(location) == 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND J.Salary_ID = @Sal";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) == 0 && int.Parse(location) != 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND P.Province_ID = @Locat";
                    }
                    else if (int.Parse(category) != 0 && int.Parse(pos) != 0 && int.Parse(location) == 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE CA.Category_ID = @Cat AND J.Position_ID = @Pos";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) != 0 && int.Parse(location) != 0 && int.Parse(salary) == 0)
                    {
                        sql = "WHERE J.Position_ID = @Pos AND P.Province_ID = @Locat";
                    }
                    else if (int.Parse(category) == 0 && int.Parse(pos) != 0 && int.Parse(location) == 0 && int.Parse(salary) != 0)
                    {
                        sql = "WHERE J.Position_ID = @Pos AND J.Salary_ID = @Sal";
                    }
                    else
                    {
                        sql = "";
                    }

                    cmd.CommandText = command + sql;
                    cmd.Parameters.AddWithValue("@Cat", category);
                    cmd.Parameters.AddWithValue("@Pos", pos);
                    cmd.Parameters.AddWithValue("@Locat", location);
                    cmd.Parameters.AddWithValue("@Sal", salary);
                    cmd.Connection = con;
                    con.Open();
                    mydata = new SqlDataAdapter(cmd);
                    mydata.Fill(dt);
                    mydata.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            JobSearchList.Add(new bl_job()
                            {
                                ID = item["ID"].ToString(),
                                CompanyID = item["Company_ID"].ToString(),
                                CompanyName = item["Company_Name"].ToString(),
                                LOGO = item["Logo"].ToString(),
                                Position = item["Position_Name"].ToString(),
                                Category = item["Category_Name"].ToString(),
                                JobType = item["Job_Type"].ToString(),
                                Priority = item["Status"].ToString(),
                                Schedule = item["Schedule"].ToString(),
                                Province = item["Province_Name"].ToString(),
                                Salary = item["Salary"].ToString(),
                                CreatedOn = item["Created_On"].ToString(),
                                CreatedBy = item["Created_By"].ToString(),
                                ExpiredOn = item["Expired_Date"].ToString(),

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return JobSearchList;
            }

            return JobSearchList;
        }

        public static int JobLikeStatus(string username, string job_id)
        {
            int status = 0;
            DataTable dt = new DataTable();
            SqlDataAdapter mydata;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Job_Like WHERE UserID = @User AND Job_ID = @ID";
                    cmd.Parameters.AddWithValue("@User", username);
                    cmd.Parameters.AddWithValue("@ID", job_id);
                    cmd.Connection = con;
                    con.Open();
                    mydata = new SqlDataAdapter(cmd);
                    mydata.Fill(dt);
                    mydata.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        return status = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                return status;
            }

            return status;
        }

        public static int OnJobLike(string username, string job_id)
        {
            int status = 0;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO JP_TBL_Job_Like(UserID, Job_ID, Created_On) VALUES(@User, @ID, @CreatedOn)";
                    cmd.Parameters.AddWithValue("@User", username);
                    cmd.Parameters.AddWithValue("@ID", job_id);
                    cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.Date);
                    cmd.Connection = con;
                    con.Open();
                    status = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                return status;
            }

            return status;
        }

        public static int OnJobUnLike(string username, string job_id)
        {
            int status = 0;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM JP_TBL_Job_Like WHERE UserID = @User AND Job_ID = @ID ";
                    cmd.Parameters.AddWithValue("@User", username);
                    cmd.Parameters.AddWithValue("@ID", job_id);
                    cmd.Connection = con;
                    con.Open();
                    status = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                return status;
            }

            return status;
        }

        public static DataTable countJobLike(string job_id)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter mydata;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Job_Like WHERE Job_ID = @ID";
                    cmd.Parameters.AddWithValue("@ID", job_id);
                    cmd.Connection = con;
                    con.Open();
                    mydata = new SqlDataAdapter(cmd);
                    mydata.Fill(dt);
                    mydata.Dispose();
                    cmd.Dispose();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                return dt;
            }

            return dt;
        }

        public static bool OnSendCV(string job_id, string user_id, string file_cv)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO JP_TBL_Candidate(UserID, Job_ID, App_Form, Created_On) VALUES(@USERID,@JOBID,@FILE,@CREATEDON)";
                    cmd.Parameters.AddWithValue("@JOBID", job_id);
                    cmd.Parameters.AddWithValue("@USERID", user_id);
                    cmd.Parameters.AddWithValue("@FILE", file_cv);
                    cmd.Parameters.AddWithValue("@CREATEDON", DateTime.Now.Date);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                return status;
            }

            return true;
        }

        public static List<bl_job> GetJobReportList()
        {
            List<bl_job> reportList = new List<bl_job>();
            DataTable dt = new DataTable();
            SqlDataAdapter mydata;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Job J INNER JOIN JP_TBL_Position PS ON PS.Position_ID = J.Position_ID "
                                       + "INNER JOIN JP_TBL_Company C ON C.Company_ID = J.Company_ID "
                                       + "INNER JOIN JP_TBL_Province P ON P.Province_ID = C.Province_ID "
                                       + "INNER JOIN JP_TBL_Category CA  ON CA.Category_ID = PS.Category_ID ";
                    cmd.Connection = con;
                    con.Open();
                    mydata = new SqlDataAdapter(cmd);
                    mydata.Fill(dt);
                    mydata.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            reportList.Add(new bl_job()
                            {
                                CompanyName = item["Company_Name"].ToString(),
                                Position = item["Position_Name"].ToString(),
                                Category = item["Category_Name"].ToString(),
                                Province = item["Province_Name"].ToString(),
                                CreatedOn = item["Created_On"].ToString(),
                                ExpiredOn = item["Expired_Date"].ToString(),

                                LikeAmount = GetCountLikeByID(Convert.ToInt32(item["ID"].ToString())),
                                CandidateCV = GetCountCandidateByID(Convert.ToInt32(item["ID"].ToString()))
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return reportList;
        }

        public static int GetCountLikeByID(int job_id)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter my_da;
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Job_Like JL "
                                    + "INNER JOIN JP_TBL_Job J on J.ID = JL.Job_ID "
                                    + "WHERE J.ID = @ID";
                    cmd.Parameters.AddWithValue("@ID", job_id);
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows.Count;
                    }
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }
        public static int GetCountCandidateByID(int job_id)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter my_da;
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Candidate CAN "
                                    + "INNER JOIN JP_TBL_Job J on J.ID = CAN.Job_ID "
                                    + "WHERE J.ID = @ID";
                    cmd.Parameters.AddWithValue("@ID", job_id);
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows.Count;
                    }
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        public static List<bl_job> GetReationInfo()
        {
            List<bl_job> ReationList = new List<bl_job>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 4 J.ID, Fullname, Profiles, Position_Name, JL.Created_On FROM JP_TBL_Job_Like JL " 
                                        + "INNER JOIN JP_TBL_Job J ON JL.Job_ID = J.ID "
                                        + "INNER JOIN JP_TBL_Position PS ON PS.Position_ID = J.Position_ID "
                                        + "INNER JOIN JP_TBL_Users U ON U.UserID = JL.UserID "
                                        + "ORDER BY JL.Created_On DESC ";
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow job in dt.Rows)
                        {
                            ReationList.Add(new bl_job
                            {
                                ID = Convert.ToInt32(job["ID"]).ToString(),
                                User = job["Fullname"].ToString(),
                                Profiles = job["Profiles"].ToString(),
                                Position = job["Position_Name"].ToString(),
                                CreatedOn = job["Created_On"].ToString(),
                                LikeAmount = GetCountLikeByID(Convert.ToInt32(job["ID"].ToString())),
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return ReationList;
        }

    }
}