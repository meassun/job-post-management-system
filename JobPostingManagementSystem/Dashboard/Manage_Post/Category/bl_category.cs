﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Manage_Post.Category
{
    public class bl_category
    {
        public bl_category()
        {

        }

        #region //Properties bl_category
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string OfPosition { get; set; }
        public string Description { get; set; }
        public int availablePosition { get; set; }
        #endregion
    }
}