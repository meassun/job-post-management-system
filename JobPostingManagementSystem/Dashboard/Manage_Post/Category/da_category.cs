﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Manage_Post.Category
{
    public class da_category
    {
        public da_category()
        { 
        }

        public static List<bl_category> GetCategoryList()
        {
            List<bl_category> categoryList = new List<bl_category>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Category";
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            categoryList.Add(new bl_category
                            {
                                CategoryID = Convert.ToInt32(item["Category_ID"].ToString()),
                                CategoryName = item["Category_Name"].ToString(),
                                Description = item["Descript"].ToString(),
                                availablePosition = GetCountPosByCategoryID(Convert.ToInt32(item["Category_ID"].ToString()))
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return categoryList;
        }

        public static List<bl_category> GetAvailableJobByCategory()
        {
            List<bl_category> Availablecategory = new List<bl_category>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Category ";
                    //cmd.CommandText = "SELECT * FROM JP_TBL_Category "
                    //                + "INNER JOIN JP_TBL_Position ON JP_TBL_Position.Category_ID = JP_TBL_Category.Category_ID "
                    //                + "INNER JOIN JP_TBL_Job ON JP_TBL_Job.Position_ID = JP_TBL_Position.Position_ID"
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            if (GetCountPosByCategoryID(Convert.ToInt32(item["Category_ID"].ToString())) != 0)
                            {
                                Availablecategory.Add(new bl_category
                                {
                                    CategoryID = Convert.ToInt32(item["Category_ID"].ToString()),
                                    CategoryName = item["Category_Name"].ToString(),
                                    Description = item["Descript"].ToString(),
                                    availablePosition = GetCountPosByCategoryID(Convert.ToInt32(item["Category_ID"].ToString()))
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return Availablecategory;
        }

        public static bool AddCategory(string cname, string desc)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO JP_TBL_Category(Category_Name, Descript) VALUES(@CategoryName, @Descript)";
                    cmd.Parameters.AddWithValue("@CategoryName", cname);
                    cmd.Parameters.AddWithValue("@Descript", desc);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }
            
        }
        public static bool GetCategoryByName(string category_name)
        {
            bool result = true;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Category WHERE Category_Name = @CategoryName";
                    cmd.Parameters.AddWithValue("@CategoryName", category_name);
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                        return result;
                    else
                        return !result;
                }
            }
            catch (Exception)
            {
                return !result;
            }
        }



        public static List<bl_category> GetCategoryByID(int category_id)
        {
            List<bl_category> categoryList = new List<bl_category>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Category WHERE Category_ID = @CategoryID";
                    cmd.Parameters.AddWithValue("@CategoryID", category_id);
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            categoryList.Add(new bl_category
                            {
                                CategoryName = item["Category_Name"].ToString(),
                                Description = item["Descript"].ToString(),
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return categoryList;
        }
        public static int UpdateCategory(string category_id, string category_name, string descript)
        {
            int status = 0;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE JP_TBL_Category SET Category_Name=@CategoryName, Descript= @CategoryDesc WHERE Category_ID = @CategoryID";
                    cmd.Parameters.AddWithValue("@CategoryID", category_id);
                    cmd.Parameters.AddWithValue("@CategoryName", category_name);
                    cmd.Parameters.AddWithValue("@CategoryDesc", descript);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return 1;
                }
            }
            catch (Exception)
            {
                return status;
            }
        }
        
        public static int DeleteCategory(int category_id)
        {
            int status = 0;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlCommand cmd1 = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM JP_TBL_Position WHERE Category_ID = @CategoryID";
                    cmd1.CommandText = "DELETE FROM JP_TBL_Category WHERE Category_ID = @CategoryID";
                    cmd.Parameters.AddWithValue("@CategoryID", category_id);
                    cmd1.Parameters.AddWithValue("@CategoryID", category_id);
                    cmd.Connection = con;
                    cmd1.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    con.Close();

                    return 1;
                }
            }
            catch (Exception)
            {
                return status;
            }
        }


        public static int GetCountPosByCategoryID(int category_id)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter my_da;
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Position P "
                                    + "INNER JOIN JP_TBL_Job J on J.Position_ID = P.Position_ID "
                                    + "WHERE Category_ID = @CategoryID";
                    cmd.Parameters.AddWithValue("@CategoryID", category_id);
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows.Count;
                    }
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }


    }
}