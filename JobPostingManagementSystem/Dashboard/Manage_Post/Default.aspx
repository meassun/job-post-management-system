﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="Default.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Manage_Post.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DashContent" runat="server">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetCategoryList();
            GetPositionList();
        });
        var CatID = 0;
        var PosID = 0;

        function GetCategoryList() {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetCategoryList",
                contentType: "application/json; charset=utf-8",
                data: {},
                dataType: "json",
                success: function (response) {
                    var categoryList = response.d;
                    var count = 0;
                    $.each(categoryList, function (index, category) {
                        count += 1;
                        $("#view-category tbody").append('<tr><td>' + count + '</td><td>'
                        + category.CategoryID + '</td><td>'
                        + category.CategoryName + '</td><td>'
                        + category.availablePosition + '</td><td>'
                        + category.Description + '</td><td class="text-center"><button type="button" class="btn btn-xs btn-primary" onClick="EditCategory(' + category.CategoryID + ')"><i class="fa fa-pencil-square-o text-default text-center"></i> Edit</button>' +
                        '&nbsp;&nbsp;<button type="button" class="btn btn-xs btn-danger" onClick="DeleteCategory(' + category.CategoryID + ')"><i class="fa fa-trash text-default text-center"></i> Del</button></td></tr>');
                    });
                }
            });
        }

        function EditCategory(CategoryID) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetCategoryByID",
                contentType: "application/json; charset=utf-8",
                data: '{category_id:' + CategoryID + '}',
                dataType: "json",
                success: function (response) {
                    var CList = response.d;
                    $.each(CList, function (index, category) {
                        CatID = CategoryID;
                        $('#<%=txtEdtCname.ClientID%>').val(category.CategoryName);
                        $('#<%=txtEdtDesc.ClientID%>').val(category.Description);
                    });
                        
                    ShowCategory();
                }
            });
        }
        function OnUpdateCategory() {
            var CategoryName = $("#<%=txtEdtCname.ClientID%>").val();
            var Descript = $("#<%=txtEdtDesc.ClientID%>").val();
            var CList = { id: CatID, categoryName: CategoryName, descript: Descript };
            var data = JSON.stringify(CList);

            $.ajax({
                type: "POST",
                url: "Default.aspx/SaveEditCategory",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                success: function (status) {
                    if (status.d != 0) {
                        $("#view-category tbody").empty();
                        GetCategoryList();
                        showAlert("Category has been updated.", "alert-success");
                    }
                    else {
                        showAlert("Updatting failed! try again.", "alert-danger");
                    }
                }
            });
        }
        function DeleteCategory(CategoryID) {
            var con = confirm("Are you sure want to delete?");
            if (con == true) {
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/DeleteCategory",
                    contentType: "application/json; charset=utf-8",
                    data: '{category_id:' + CategoryID + '}',
                    dataType: "json",
                    success: function (response) {
                        var status = response.d;
                        if (status != 0) {
                            $("#view-category tbody").empty();
                            GetCategoryList();
                            showAlert("Category has been deleted.", "alert-success");
                        } else {
                            showAlert("Delete category failed, try again!", "alert-danger");
                        }
                    }
                });
            }
        }

        function GetPositionList() {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetPositionList",
                contentType: "application/json; charset=utf-8",
                data: {},
                dataType: "json",
                success: function (response) {
                    var PList = response.d;
                    var count = 0;
                    $.each(PList, function (index, position) {
                        count += 1;
                        $("#view-position").append('<tr><td>' + count + '</td><td>'
                            + position.PositionID + '</td><td>'
                            + position.PositionName + '</td><td>'
                            + position.OfCategory + '</td><td class="text-center"><button type="button" class="btn btn-xs btn-primary" onClick="EditPosition(' + position.PositionID + ')"><i class="fa fa-pencil-square-o text-default text-center"></i> Edit</button>' +
                        '&nbsp;&nbsp;<button type="button" class="btn btn-xs btn-danger" onClick="DeletePosition(' + position.PositionID + ')"><i class="fa fa-trash text-default text-center"></i> Del</button></td></tr>');
                    });
                }
            });
        }

        function EditPosition(PositionID) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetPositionByID",
                contentType: "application/json; charset=utf-8",
                data: '{position_id:' + PositionID + '}',
                dataType: "json",
                success: function (response) {
                    var PList = response.d;
                    $.each(PList, function (index, position) {
                        PosID = PositionID;
                        $('#<%=txtEdtPosition.ClientID%>').val(position.PositionName);
                        $('#<%=ddlEdtCategory.ClientID%>').val(position.OfCategory);
                    });
                        
                    ShowPosition();
                }
            });
        }
        function OnUpdatePosition() {
            var PositionName = $("#<%=txtEdtPosition.ClientID%>").val();
            var Category = $("#<%=ddlEdtCategory.ClientID%>").val();
            var PList = { id: PosID, positionName: PositionName, category: Category };
            var data = JSON.stringify(PList);

            $.ajax({
                type: "POST",
                url: "Default.aspx/SaveEditPosition",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                success: function (status) {
                    if (status.d != 0) {
                        $("#view-position tbody").empty();
                        GetPositionList();
                        showAlert("Position has been updated.", "alert-success");
                    }
                    else {
                        showAlert("Updatting failed! try again.", "alert-danger");
                    }
                }
            });
        }
        function DeletePosition(PositionID) {
            var con = confirm("Are you sure want to delete?");
            if (con == true) {
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/DeletePosition",
                    contentType: "application/json; charset=utf-8",
                    data: '{position_id:' + PositionID + '}',
                    dataType: "json",
                    success: function (response) {
                        var status = response.d;
                        if (status != 0) {
                            $("#view-position tbody").empty();
                            GetPositionList();
                            showAlert("Position has been deleted.", "alert-success");
                        } else {
                            showAlert("Delete position failed, try again!", "alert-danger");
                        }
                    }
                });
            }
        }
        
        function showAlert(message, alert) {
            $("#alert").show();
            $("#alert").addClass(alert);
                $("p").text(message);
                window.setTimeout(function () {
                    $("#alert").fadeOut(100, function () {
                        $("#alert").hide();
                    });
                }, 4000);
        };

        function ShowCategory() {
            $("#btnShowCategory").click();
        }
        function ShowPosition() {
            $("#btnShowPosition").click();
        }
        

    </script>
    <div id="page-wrapper">
        <div class="container-fluid">
            <br />
            <br />
            <div class="row">
                <div class="col-md-2">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="m-t-small">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addCategoryModal"><i class="fa fa-fw fa-plus"></i>ADD CATEGORY</button>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="m-t-small">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addPositionModal"><i class="fa fa-fw fa-plus"></i>ADD POSITION</button>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
            <br />
            <div class="row">
                <div class="alert" id="alert" role="alert" hidden>
                    <p></p>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Manage Posting
                        </div>
                        <div class="panel-body">
                            <section class="content">
                                <div class="box box-info">
                                    <div class="row" style="margin-right: 0.2%; margin-left: 0.2%; margin-top: 1%;">
                                        <div class="col-sm-12 table-responsive">

                                            <div id="view-category_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                                <div class="pull-left">
                                                        <label><span class="label label-default">+ Category List</span></label>
                                                </div>
                                                <table id="view-category" class="table table-condensed table-hover dataTable" style="width: 100%; border: 1pt groove #ffffff;" role="grid" aria-describedby="view-loans_info">
                                                    <thead>
                                                        <tr style="background-color: #D1F9FF" role="row">
                                                            <th class="sorting" style="width: 82px;">#No</th>
                                                            <th class="sorting" style="width: 82px;">Category ID</th>
                                                            <th class="sorting" style="width: 122px;">Category Name</th>
                                                            <th class="sorting" style="width: 122px;">#Of Position</th>
                                                            <th style="width: 58px;">Description</th>
                                                            <th class="text-center" style="width: 60px;">Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tfoot class="bg-gray">
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>

                                            </div>

                                            <div id="view-Position_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                                <div class="pull-left">
                                                        <label><span class="label label-default">+ Position List</span></label>
                                                </div>
                                                <table id="view-position" class="table table-condensed table-hover dataTable" style="width: 100%; border: 1pt groove #ffffff;" role="grid" aria-describedby="view-loans_info">
                                                    <thead>
                                                        <tr style="background-color: #D1F9FF" role="row">
                                                            <th class="sorting" style="width: 82px;">#No</th>
                                                            <th class="sorting" style="width: 82px;">Position ID</th>
                                                            <th class="sorting" style="width: 122px;">Position Name</th>
                                                            <th class="sorting" style="width: 122px;">#Of Category</th>
                                                            <th style="width: 60px;">Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tfoot class="bg-gray">
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </section>
                            <button type="button" style="display: none;" id="btnShowCategory" class="btn btn-primary btn-lg"
                                data-toggle="modal" data-target="#UpdateCategoryModal">
                            </button>
                            <button type="button" style="display: none;" id="btnShowPosition" class="btn btn-primary btn-lg"
                                data-toggle="modal" data-target="#UpdatePositionModal">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<%--The Category Add Modal--%>
    <div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Add Category</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtCname" runat="server" class="form-control" placeholder="Category name..."></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtDescript" class="form-control" TextMode="MultiLine" Columns="20" Rows="3" runat="server" placeholder="Description..." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnAddCategory" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Save" OnClick="btnAddCategory_Click" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>--%>

    <%--The Category Add Modal--%>
    <div class="modal fade" id="addPositionModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Add Position</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtPosName" runat="server" class="form-control" placeholder="Position name..."></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="CategoryList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceCategory" DataTextField="Category_Name" DataValueField="Category_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Category--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceCategory" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Category ORDER BY Category_ID ASC"></asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnAddPosition" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Save" OnClick="btnAddPosition_Click" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <%--The Category Update Modal--%>
    <div class="modal fade" id="UpdateCategoryModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Edit Category</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtEdtCname" runat="server" class="form-control" placeholder="Category name..."></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtEdtDesc" class="form-control" TextMode="MultiLine" Columns="20" Rows="3" runat="server" placeholder="Description..." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="button" id="btnEditCategory" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" Style="border: 1pt groove #d5d5d5;" onclick="OnUpdateCategory()">Save</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>

     <%--The Position Update Modal--%>
    <div class="modal fade" id="UpdatePositionModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Edit Position</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:TextBox ID="txtEdtPosition" runat="server" class="form-control" placeholder="Position name..."></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlEdtCategory" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceEdtCategory" DataTextField="Category_Name" DataValueField="Category_ID">
                                    <asp:ListItem Selected="True" Value="0">--All Category--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSourceEdtCategory" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Category ORDER BY Category_ID ASC"></asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button id="btnEditPosition" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" Style="border: 1pt groove #d5d5d5;" onclick="OnUpdatePosition()"> Save</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>