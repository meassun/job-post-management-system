﻿using JobPostingManagementSystem.Dashboard.Manage_Post.Category;
using JobPostingManagementSystem.Dashboard.Manage_Post.Position;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard.Manage_Post
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<bl_category> GetCategoryList()
        {
            List<bl_category> categoryList = new List<bl_category>();
            categoryList = da_category.GetCategoryList();

            return categoryList;
        }
        

        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            try
            {
                string categoryName, Descrition;
                categoryName = txtCname.Text.Trim();
                Descrition = txtDescript.Text.Trim();
                if (categoryName != "")
                {
                    bool status = false;
                    if (da_category.GetCategoryByName(categoryName) != true)
                    {
                        status = da_category.AddCategory(categoryName, Descrition);
                        if (status != false)
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "New category has been added successfully." + "'" + "," + " '" + "alert-success" + "');", true);
                        else
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Add new category failed, Please try again!" + "'" + "," + " '" + "alert-danger" + "');", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Category is existing, please try again!" + "'" + "," + " '" + "alert-warning" + "');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Please check the information again!" + "'"+","+" '"+ "alert-warning" +"');", true);
                }
            }
            catch (Exception)
            {
            }
        }

        [WebMethod]
        public static List<bl_position> GetPositionList()
        {
            List<bl_position> positionList = new List<bl_position>();
            positionList = da_position.GetPositionList();

            return positionList;
        }

        protected void btnAddPosition_Click(object sender, EventArgs e)
        {
            try
            {
                string positionName, Category;
                positionName = txtPosName.Text.Trim();
                Category = CategoryList.SelectedValue.ToString();
                if (positionName != "")
                {
                    bool status = false;
                    if (da_position.GetPositionByName(positionName) != true)
                    {
                        status = da_position.AddPosition(positionName, Category);
                        if (status != false)
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "New position has been added successfully." + "'" + "," + " '" + "alert-success" + "');", true);
                        else
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Add new position failed, Please try again!" + "'" + "," + " '" + "alert-danger" + "');", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Position is existing, please try again!" + "'" + "," + " '" + "alert-warning" + "');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "Please check the information again!" + "'" + "," + " '" + "alert-warning" + "');", true);
                }
            }
            catch (Exception)
            {
            }
        }

        [WebMethod]
        public static List<bl_category> GetCategoryByID(int category_id)
        {
            List<bl_category> categoryList = new List<bl_category>();
            categoryList = da_category.GetCategoryByID(category_id);

            return categoryList;

        }

        [WebMethod]
        public static int SaveEditCategory(string id, string categoryName, string descript)
        {
            int status = 0;
            status = da_category.UpdateCategory(id, categoryName, descript);
            if (status != 0)
                return 1;
            else
                return status;
        }

        [WebMethod]
        public static int DeleteCategory(int category_id)
        {
            int status = 0;
            status = da_category.DeleteCategory(category_id);
            if (status != 0)
                return 1;
            else
                return 0;
        }

        [WebMethod]
        public static List<bl_position> GetPositionByID(int position_id)
        {
            List<bl_position> positionList = new List<bl_position>();
            positionList = da_position.GetPositionByID(position_id);

            return positionList;

        }

        [WebMethod]
        public static int SaveEditPosition(string id, string positionName, string category)
        {
            int status = 0;
            status = da_position.UpdatePosition(id, positionName, category);
            if (status != 0)
                return 1;
            else
                return status;
        }

        [WebMethod]
        public static int DeletePosition(int position_id)
        {
            int status = 0;
            status = da_position.DeletePosition(position_id);
            if (status != 0)
                return 1;
            else
                return 0;
        }
    }
}