﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Manage_Post.Position
{
    public class bl_position
    {
        public bl_position()
        {

        }

        #region //Properties bl_position
        public int PositionID { get; set; }
        public string PositionName { get; set; }
        public string OfCategory { get; set; }
        #endregion
    }
}