﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Manage_Post.Position
{
    public class da_position
    {
        public da_position()
        { 
        
        }


        public static List<bl_position> GetPositionList()
        {
            List<bl_position> positionList = new List<bl_position>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT A.*, B.Category_Name FROM JP_TBL_Position A INNER JOIN JP_TBL_Category B ON A.Category_ID = B.Category_ID ";
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            positionList.Add(new bl_position
                            {
                                PositionID = Convert.ToInt32(item["Position_ID"].ToString()),
                                PositionName = item["Position_Name"].ToString(),
                                OfCategory = item["Category_Name"].ToString(),
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return positionList;
        }

        public static bool GetPositionByName(string pos_name)
        {
            bool result = true;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Position WHERE Position_Name = @PosName";
                    cmd.Parameters.AddWithValue("@PosName", pos_name);
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                        return result;
                    else
                        return !result;
                }
            }
            catch (Exception)
            {
                return !result;
            }
        }


        public static bool AddPosition(string pos_name, string category)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO JP_TBL_Position(Position_Name, Category_ID) VALUES(@PosName, @Category)";
                    cmd.Parameters.AddWithValue("@PosName", pos_name);
                    cmd.Parameters.AddWithValue("@Category", category);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }

        }

        public static List<bl_position> GetPositionByID(int position_id)
        {
            List<bl_position> positionList = new List<bl_position>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_da;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Position WHERE Position_ID = @PositionID";
                    cmd.Parameters.AddWithValue("@PositionID", position_id);
                    cmd.Connection = con;
                    con.Open();
                    my_da = new SqlDataAdapter(cmd);
                    my_da.Fill(dt);
                    my_da.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            positionList.Add(new bl_position
                            {
                                PositionName = item["Position_Name"].ToString(),
                                OfCategory = item["Category_ID"].ToString(),
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return positionList;
        }

        public static int UpdatePosition(string position_id, string position_name, string category)
        {
            int status = 0;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE JP_TBL_Position SET Position_Name=@PositionName, Category_ID= @Category WHERE Position_ID = @PositionID";
                    cmd.Parameters.AddWithValue("@PositionID", position_id);
                    cmd.Parameters.AddWithValue("@PositionName", position_name);
                    cmd.Parameters.AddWithValue("@Category", category);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    return 1;
                }
            }
            catch (Exception)
            {
                return status;
            }
        }



        public static int DeletePosition(int position_id)
        {
            int status = 0;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlCommand cmd1 = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM JP_TBL_Job WHERE Position_ID = @PositionID";
                    cmd1.CommandText = "DELETE FROM JP_TBL_Position WHERE Position_ID = @PositionID";
                    cmd.Parameters.AddWithValue("@PositionID", position_id);
                    cmd.Parameters.AddWithValue("@PositionID", position_id);
                    cmd.Connection = con;
                    cmd1.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    con.Close();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return status;
            }
        }



    }
}