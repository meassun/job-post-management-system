﻿<%@ Page Language="C#" MasterPageFile="~/Dashboard/Dash.Master" AutoEventWireup="true" CodeBehind="Job_Report.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Report.Job_Report" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="DashContent">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            getAllJobReport();
        });
       
        var JobID = 0;
        function getAllJobReport()
        {
            $.ajax({
                type: "POST",
                url: "Job_Report.aspx/GetJobReportList",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var JobReport = response.d;
                    var count = 0;
                    $.each(JobReport, function (index, job) {
                        count += 1;
                        $("#view-job tbody").append('<tr id= '+job.ID+'><td>' + count + '</td><td>'
                            + job.Position + '</td><td>' + job.CompanyName + '</td><td>'
                            + job.Category + '</td><td>' + job.Province + '</td><td style="text-align: center;">'
                            + job.LikeAmount + '  <i class="fa fa-thumbs-up" aria-hidden="true" style="color: rgb(32, 120, 244);"></i></td><td style="text-align: center;">'
                            + job.CandidateCV + '  <span class="fa fa-paperclip" style="font-size: 130%; "></span></td><td>'
                            + job.ExpiredOn + '</td></td></tr>');
                    });
                },
                error: function () {

                }
            });
        }

        function showAlert(message, alert) {
            $("#alert").show();
            $("#alert").addClass(alert);
            $("p").text(message);
            window.setTimeout(function () {
                $("#alert").fadeOut(100, function () {
                    $("#alert").hide();
                });
            }, 6000);
        };

        function ShowPopup() {
            $("#btnShowPopup").click();
        }
    </script>
  
    <div id="page-wrapper">
        <div class="container-fluid">
            <br />
            <br />
            <div class="row">
                <div class="col-md-2">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="m-t-small">
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#ExportReport"><i class="fa fa-file-excel-o"></i> <b>Export</b></button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="alert" id="alert" role="alert" hidden>
                    <p></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12"> 
                    <section class="content">
                        <div class="box box-info">
                            <div class="row" style="margin-right: 0.2%; margin-top: 0%;">
                                <div class="col-sm-12 table-responsive">
                                    <div id="view-job_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                        <div class="pull-right">
                                            <div id="view-job_filter" class="dataTables_filter">
                                                <label><input type="search" class="form-control input-sm" placeholder="Search Job" aria-controls="view-loans"></label>
                                            </div>
                                        </div>
                                        <table id="view-job" class="table table-condensed table-hover dataTable" style="width: 100%; border: 1pt groove #ffffff;" role="grid">
                                            <thead>
                                                <tr style="background-color: #D1F9FF" role="row">
                                                    <th class="sorting" style="width: 35px;">No</th>
                                                    <th class="sorting" style="width: 82px;">Position</th>
                                                    <th class="sorting" style="width: 110px;">Category</th>
                                                    <th style="width: 58px;">Company</th>
                                                    <th style="width: 74px;">Location</th>
                                                    <th style="width: 90px;">Like Amount</th>
                                                    <th style="width: 74px;">CV Upload</th>
                                                    <th class="sorting" style="width: 121px;">Close Date</th>
                                                </tr>
                                            </thead>

                                            <tfoot class="bg-gray">
                                            </tfoot>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                        <div class="dataTables_info" id="view-job_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
                                        <div class="dataTables_paginate paging_simple_numbers" id="view-job_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button previous disabled" id="view-job_previous"><a href="#" aria-controls="view-job" data-dt-idx="0" tabindex="0">Previous</a></li>
                                                <li class="paginate_button active"><a href="#" aria-controls="view-job" data-dt-idx="1" tabindex="0">1</a></li>
                                                <li class="paginate_button next disabled" id="view-job_next"><a href="#" aria-controls="view-job" data-dt-idx="2" tabindex="0">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>

                    <button type="button" style="display: none;" id="btnShowPopup" class="btn btn-primary btn-lg"
                        data-toggle="modal" data-target="#EdtJobModal">
                    </button>
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                </div>
            </div>
        </div>
    </div>
    <!-- The modal -->
</asp:Content>
