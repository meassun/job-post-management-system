﻿using JobPostingManagementSystem.Dashboard.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard.Report
{
    public partial class Job_Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<bl_job> GetJobReportList()
        {
            List<bl_job> JobReport = new List<bl_job>();
            JobReport = da_job.GetJobReportList();

            return JobReport;
        }
    }
}