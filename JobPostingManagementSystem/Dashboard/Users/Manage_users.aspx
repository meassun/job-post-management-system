﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Dashboard/Dash.Master" CodeBehind="Manage_users.aspx.cs" Inherits="JobPostingManagementSystem.Dashboard.Users.Manage_users" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="DashContent">
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }
        .overlay {
          position: absolute;
          bottom: 0;
          left: 0;
          right: 0;
          background-color: #32383a;
          overflow: hidden;
          width: 100%;
          /*height: 0;*/
          transition: .5s ease;
        }
        .edit {
          color: white;
          font-size: 20px;
          position: absolute;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          text-align: center;
        }
        .UserImage:hover .overlay {
          height: 100%;
          opacity: 0.5;
          filter: alpha(opacity=50);
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function ShowPopup() {
            $("#btnShowPopup").click();
        }

        function showAlert(errorStatus) {
            var message = "";
            $("#alert").show();
            if (errorStatus == "A") {
                message = "Add new user failed, Please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "B") {
                message = "Please check the information again!";
                $("#alert").show();
                $("#alert").addClass("alert-warning");
                $("p").text(message);
            }
            else if (errorStatus == "C") {
                message = "Update user failed, please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "D") {
                message = "Delete user failed, please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else if (errorStatus == "E") {
                message = "Cannot find user to delete, please try again!";
                $("#alert").addClass("alert-danger");
                $("p").text(message);
            }
            else {
                message = "New user has been added successfully.";
                $("#alert").addClass("alert-success");
                $("p").text(message);
            }

            window.setTimeout(function () {
                $("#alert").fadeOut(100, function () {
                    $("#alert").hide();
                });
            }, 4000);
        };

    </script>
    <div id="page-wrapper">
        <div class="container-fluid" id="User_List" runat="server">
            <br />
            <br />
            <div class="row">
                <div class="col-md-9">
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="m-t-small">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addUserModal"><i class="fa fa-fw fa-plus"></i>ADD New User</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="alert" id="alert" role="alert" hidden>
                    <p></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>User List
                        </div>
                        <div class="panel-body">
                            <asp:GridView ID="Gv_UserList" runat="server" AutoGenerateColumns="False" DataKeyNames="UserID"
                                CssClass="footable" Style="max-width: 100%" AllowPaging="True" PageSize="10" AllowSorting="True" OnPageIndexChanging="Gv_UserList_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Profile">
                                        <ItemTemplate>
                                            <asp:Image ID="Image" runat="server" ImageUrl='<%# "../../Images/User/" + Eval("Profile")%>' Height="55px" Width="60px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Fullname" HeaderText="Full Name" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Username" HeaderText="User Name" SortExpression="Username" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Email" HeaderText="EMAIL" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Role" HeaderText="Authentication" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="ACTION">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" class="form-control btn btn-primary" runat="server" CausesValidation="false" CommandName="Edits"
                                                CommandArgument='<%# Eval("UserID") %>' OnClick="GetUserInfo_Click"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <button type="button" style="display: none;" id="btnShowPopup" class="btn btn-primary btn-lg"
                                data-toggle="modal" data-target="#EdtUserModal">
                                Modal
                            </button>
                            <asp:ScriptManager runat="server"></asp:ScriptManager>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <%--Success Registered Panel--%>
            <div class="row" id="Success_Create_panel" runat="server">
                <br />
                <br />
                <br />
                <br />
                <div class="col-md-12 ">
                    <div class="regist-panel  panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center text-uppercase" style="font-weight: bold;">Registeration</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <p>
                                            Dear
                                            <asp:Label ID="lblCreatedBy" runat="server" Text="" Style="text-align: center; font-weight: bold"></asp:Label><br>
                                            <br>
                                            New User: <asp:Label ID="lblUsername" runat="server" Style="text-align: center; font-weight: bold"/> have been registered.
                                        </p>
                                        <p>An e-mail has been sent to remind the registered user with username and password.</p>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="btn btn-info" href="../../Dashboard/Users/Manage_Users.aspx"><b>User List</b></a>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>

    <!-- The modal add user-->
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="row" id="AddUser_panel" runat="server">
                <div class="modal-content">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="panel-title text-left"><b>Registration</b></h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtFname" runat="server" class="form-control" placeholder="Full name..."></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtusername" runat="server" class="form-control" placeholder="Username..."></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email..."></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:DropDownList ID="RoleList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceRoles" DataTextField="Rolename" DataValueField="RoleID">
                                        <asp:ListItem Selected="True" Value="0">--All Role--</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSourceRoles" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Roles ORDER BY RoleID ASC"></asp:SqlDataSource>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control" placeholder="Password..."></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtConPassword" type="password" runat="server" class="form-control" placeholder="Confirm Password..."></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:fileupload id="FileUpload" runat="server" xmlns:asp="#unknown" />
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-6"></div>

                        </fieldset>
                    </div>
                    <div class="panel-footer">
                        <asp:Button ID="btnSave" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Save" OnClick="btnSave_Click" />
                        <button class="btn pull-right" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- The modal User Edit-->
    <div class="modal fade" id="EdtUserModal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="panel-title text-left">Edit User</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <fieldset>
                            <div class="col-md-12 col-sm-6 col-xs-6" style="margin-bottom: 10px;">
                                <div class="form-group">
                                    <asp:Label ID="UID" runat="server" Visible="false" />
                                    <div class="col-md-2 col-sm-2 col-xs-12 UserImage" style="padding: 10px 5px; border: 1px solid #ddd; background: #fff; border-radius: 4px;">
                                        <asp:Image ID="UserImage" class="img-circle" runat="server" Style="width: 100%; height: auto;" />
                                        <div class="overlay">
                                            <div class="edit">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 5%;">
                                        <b>
                                            <asp:Label ID="FullName" class="text-primary text-center" runat="server" Style="font-size: large;"></asp:Label></b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"></div>
                            <br />
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtFullName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtUsername" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="edtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:DropDownList ID="edtRoleList" runat="server" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataRole" DataTextField="Rolename" DataValueField="RoleID">
                                        <asp:ListItem Selected="True" Value="0">--All Role--</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataRole" runat="server" ConnectionString="<%$ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Roles ORDER BY RoleID ASC"></asp:SqlDataSource>
                                </div>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:fileupload id="edtFileUpload" runat="server" class="form-control" xmlns:asp="#unknown" />
                                    <asp:TextBox ID="edtImage" runat="server" class="form-control" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                        </fieldset>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnEdit" runat="server" class="btn btn-primary" Style="border: 1pt groove #d5d5d5;" Text="Edit" OnClick="btnEdit_Click" />
                    <asp:Button ID="btnDelete" runat="server" class="btn btn-danger" Style="border: 1pt groove #d5d5d5;" Text="Remove" OnClick="btnDelete_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
