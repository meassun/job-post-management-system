﻿using JobPostingManagementSystem.App_Code.DA.Users;
using JobPostingManagementSystem.Dashboard.Company;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Dashboard.Users
{
    public partial class Manage_users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Success_Create_panel.Attributes.CssStyle.Add("display", "none");
            LoadUserData();
        }

        void LoadUserData()
        {
            Gv_UserList.DataSource = da_user.GetUserList();
            Gv_UserList.DataBind(); 
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string fullName, userName, email, password, conpassword, Image, Role;
            string save_path = "~/Images/";
            fullName = txtFname.Text.Trim();
            userName = txtusername.Text.Trim();
            email = txtEmail.Text.Trim();
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);

            password = txtPassword.Text.Trim();
            conpassword = txtConPassword.Text.Trim();
            Image = Path.GetFileName(FileUpload.PostedFile.FileName);
            if (Image != "")
                FileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            Role = RoleList.SelectedValue.ToString(); //Admin user create--

            if (userName != "" && email != "" && password != "" && conpassword != "")
            {
                if (match.Success)
                {
                    if (password == conpassword)
                    {
                        //Check existing user registered
                        if (da_users.GetUserExistingRegister(userName, email) == false)
                        {
                            bool status = false;
                            status = da_users.AddNewUser(fullName, userName, email, password, Image, Role);
                            if (status == true)
                            {
                                AddUser_panel.Attributes.CssStyle.Add("display", "none");
                                User_List.Attributes.CssStyle.Add("display", "none");
                                Success_Create_panel.Attributes.CssStyle.Add("display", "block");
                                lblUsername.Text = userName;
                                lblCreatedBy.Text = Session["Username"].ToString();
                                clearSession(fullName, userName, email, password, Image, Role);
                            }
                            else
                            {
                                clearSession(fullName, userName, email, password, Image, Role);
                                alertMessage("Add User failed!");
                            }
                        }
                        else
                        {
                            alertMessage("Username is already exist!");
                        }
                    }
                    else
                    {
                        alertMessage("Password is not match!");
                    }
                }
                else
                {
                    alertMessage(email + " is Invalid Email Address");
                }

            }
            else
            {
                clearSession(fullName, userName, email, password, Image, Role);
                alertMessage("Please check username and password again!");
            }

        }


        protected void GetUserInfo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton UserID = sender as LinkButton;
                if (UserID.CommandArgument != "")
                {
                    string ID = UserID.CommandArgument; 

                    DataTable dt = da_user.GetUserInfoToEdit(ID);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow user in dt.Rows)
                        {
                            UID.Text = user["UserID"].ToString();
                            FullName.Text = user["Fullname"].ToString();
                            edtFullName.Text = user["Fullname"].ToString();
                            edtUsername.Text = user["username"].ToString();
                            edtEmail.Text = user["Email"].ToString();
                            edtRoleList.Text = user["RoleID"].ToString();
                            UserImage.ImageUrl = "~/Images/User/" + user["Profiles"].ToString();
                            edtImage.Text = user["Profiles"].ToString();
                            break;
                        }

                    }

                    //Show Modal Edit
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "ShowPopup();", true);
                }
            }
            catch (Exception)
            {

            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            string UserID, fullname, username, email, role, Image;
            UserID = UID.Text.Trim();
            fullname = edtFullName.Text.Trim();
            username = edtUsername.Text.Trim();
            email = edtEmail.Text.Trim();
            role = edtRoleList.SelectedValue.ToString();
            string save_path = "~/Images/User/";
            Image = Path.GetFileName(edtFileUpload.PostedFile.FileName);
            if (Image != "")
                edtFileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            else
                Image = edtImage.Text;
            if (username != "" && fullname != "" && email != "" && role != "")
            {
                bool status = da_user.EditUser(UserID, fullname, username, email, role, Image);
                if (status != false)
                {
                    Response.Redirect("~/Dashboard/Users/Manage_users.aspx");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "C" + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "B" + "');", true);
            }


        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int UserID = 0;
            string USID, Image;
            USID = UID.Text.Trim();
            UserID = Int32.Parse(USID);
            string save_path = "~/Images/Company/";
            Image = Path.GetFileName(edtFileUpload.PostedFile.FileName);
            if (Image != "")
                edtFileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            else
                Image = edtImage.Text;
            if (UserID > 0)
            {
                bool status = da_user.DeleteUser(UserID);
                if (status != false)
                {

                    Response.Redirect("~/Dashboard/Users/Manage_users.aspx");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "D" + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "showAlert('" + "E" + "');", true);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        void clearSession(string fullName, string userName, string email, string pass, string Image, string role)
        {
            fullName = "";
            userName = "";
            email = "";
            pass = "";
            Image = "";
            role = "";
        }

        protected void Gv_UserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Gv_UserList.PageIndex = e.NewPageIndex;
            LoadUserData();
        }

        protected void alertMessage(string message)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(message);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), message, sb.ToString());
        }


    }
}