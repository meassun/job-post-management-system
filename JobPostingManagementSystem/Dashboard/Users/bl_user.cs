﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobPostingManagementSystem.Dashboard.Users
{
    public class bl_user
    {
        public bl_user()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public string UserID { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Profile { get; set; }
        public string Role { get; set; }
        public DateTime CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
