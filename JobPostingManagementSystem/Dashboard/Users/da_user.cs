﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JobPostingManagementSystem.Dashboard.Users
{
    public class da_user
    {
        public da_user()
        { 
        
        }

        public static List<bl_user> GetUserList()
        {
            List<bl_user> userList = new List<bl_user>();
            DataTable dt = new DataTable();
            SqlDataAdapter my_usr;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM JP_TBL_Users U INNER JOIN JP_TBL_Roles R ON R.RoleID = U.RoleID";
                    cmd.Connection = con;
                    con.Open();
                    my_usr = new SqlDataAdapter(cmd);
                    my_usr.Fill(dt);
                    my_usr.Dispose();
                    cmd.Dispose();
                    con.Close();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow user in dt.Rows)
                        {
                            userList.Add(new bl_user
                            {
                                UserID = user["UserID"].ToString(),
                                FullName = user["Fullname"].ToString(),
                                UserName = user["Username"].ToString(),
                                Email = user["Email"].ToString(),
                                Profile = user["Profiles"].ToString(),
                                Role = user["Rolename"].ToString()
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            return userList;
        }

        public static DataTable GetUserInfoToEdit(string user_id)
        {
            SqlDataAdapter my_da;
            DataTable dt = new DataTable();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "JP_SP_GET_USER_TO_EDIT";
                cmd.Parameters.AddWithValue("@UserID", user_id);
                cmd.Connection = con;
                con.Open();
                my_da = new SqlDataAdapter(cmd);
                my_da.Fill(dt);
                my_da.Dispose();
                cmd.Dispose();
                con.Close();
                return dt;
            }


        }

        public static bool EditUser(string UserID, string fullname, string username, string email, string role, string image)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_EDIT_USER";
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@FullName", fullname);
                    cmd.Parameters.AddWithValue("@UserName", username);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@Role", role);
                    cmd.Parameters.AddWithValue("@Image", image);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }

        }

        public static bool DeleteUser(int user_id)
        {
            bool status = false;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "JP_SP_DELETE_USER";
                    cmd.Parameters.AddWithValue("@UserID", user_id);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    return !status;
                }
            }
            catch (Exception)
            {
                return status;
            }

        }




    }
}