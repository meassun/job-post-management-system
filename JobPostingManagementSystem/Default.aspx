﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JobPostingManagementSystem._Default" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <link href="Content/css/Public/Default.css" rel="stylesheet" />
    <style>
        .job-inner > .job-details:hover {
            background-color: #d5d6d6de;
            cursor: pointer;
        }
        .group-user-react {
            border: 1px solid #ccc;
        }
        .user-react {
            padding: 5px;
        }
        .user-react:hover {
            background-color:#bbbbbb;
        }
        .d4u {
            margin-top: 3px;
            margin-bottom: 3px;
            cursor: pointer;
            margin-left: 40px;
        }

        .num0b_like {
            margin-left: -3%;
            margin-top: 1%;
            font-size: 15px;
            cursor: pointer;
            width: 15%;
        }

        /*Display The lastest user Reation to Job Post CSS here*/
              .ipsPad_half, .ipsApp ul.ipsPad_half, .ipsApp ol.ipsPad_half {
    padding: 7px;
}
        .ipsDataList {
    width: 100%;
    display: table;
    table-layout: auto;
    position: relative;
    border-collapse: separate;
    border-spacing: 0;
    list-style: none;
    padding: 0;
    margin: 0;
}
        .ipsDataItem {
    width: 100%;
    position: relative;
    border-width: 0 0 1px 0;
    border-style: solid;
    border-color: #ebebeb;
}
        html[dir="ltr"] .ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_icon {
    padding-left: 4px;
}
body[data-pageapp="forums"] .ipsDataItem_icon {
    padding: 8px 6px;
}
html[dir="ltr"] .ipsDataItem_icon {
    padding-right: 0;
}
.ipsDataItem > .ipsPos_top {
    vertical-align: top !important;
}
.ipsPos_top {
    vertical-align: top;
}
.ipsDataItem_icon {
    width: 25px;
    min-width: 25px;
    padding-top: 12px;
}
.ipsDataItem_main, .ipsDataItem_stats, .ipsDataItem_lastPoster, .ipsDataItem_generic, .ipsDataItem_modCheck, .ipsDataItem_icon {
    display: table-cell;
    padding: 12px 10px;
    vertical-align: middle;
}
.ipsUserPhoto {
    background: #fff;
    vertical-align: middle;
    display: inline-block;
    line-height: 1px;
    position: relative;
    margin: 2px;
    border-radius: 150px;
}

a {
    color: #1875a8;
    font-weight: 400;
}
.ipsUserPhoto_tiny img, img.ipsUserPhoto_tiny, .ipsUserPhoto_tiny:after {
    width: 34px;
    height: 34px;
    border-radius: 17px;
}
img {
    border-style: none;
}

img {
    vertical-align: middle;
}
html[dir="ltr"] .ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_icon + .ipsDataItem_main {
    /* padding-left: 7px; */
}

html[dir="ltr"] .ipsWidget .ipsDataItem_main.cWidgetComments {
    padding-right: 55px;
}
.ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_main, .ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_stats, .ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_lastPoster, .ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_generic {
    display: table-cell;
    padding: 10px 0;
    vertical-align: top;
}
.ipsDataItem_main {
    width: 100%;
    margin: 0;
    vertical-align: top;
}
html[dir="ltr"] .ipsWidget .ipsDataItem_main.cWidgetComments .ipsCommentCount {
    margin-right: -55px;
    margin-left: 0;
}

html[dir="ltr"] .ipsCommentCount {
    margin-left: 10px;
}
.ipsCommentCount {
    padding: 5px 10px;
    border-radius: 3px;
    background: #353c41;
    display: inline-block;
    font-size: 12px;
    text-transform: uppercase;
    position: relative;
    text-align: center;
    line-height: 15px;
    color: #ffffff;
    margin-bottom: 3px;
}
.ipsPos_right {
    float: right;
}
.ipsContained {
    display: table;
    table-layout: fixed;
    width: 85%;
}
.ipsBadge, .ipsBadge.ipsBadge_normal {
    height: 17px;
    line-height: 17px;
    font-size: 10px;
}
.ipsBadge {
    padding: 0 8px;
    border-radius: 2px;
    font-weight: 500;
    display: inline-block;
    color: #fff;
    vertical-align: middle;
    text-shadow: none;
    letter-spacing: 0;
}
.ipsDataList.ipsDataList_reducedSpacing .ipsDataItem_title {
    margin: 0;
    display: inline;
}

body[data-pageapp="forums"] .ipsDataItem_title {
    font-size: 14px;
}
.ipsType_medium:not( .ipsType_richText ) {
    line-height: 20px;
}

.ipsType_normal, .ipsType_medium {
    font-size: 14px;
}
.ipsType_medium {
    font-size: 13px;
}
.ipsType_reset {
    margin: 0;
}

.ipsDataItem .ipsDataItem_main .ipsContained > span:not(.ipsBadge) {
    padding-right: 3px;
}

.ipsType_light {
    color: #8d9aa6;
}
html[dir="ltr"] .ipsWidget .ipsDataItem_main.cWidgetComments .ipsCommentCount {
    margin-right: -55px;
    margin-left: 0;
}
        
    </style>
    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/alertify.min.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/alertify.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetLatestCompanyJobList();
            GetJobByCategoryList();
            GetUserReationPOST();
            
        });

        function GetLatestCompanyJobList()
        {
            $.ajax({
                type: "POST",
                url: 'Default.aspx/GetLatestCompanyJobPostList',
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var latestCompanyJobPostList = response.d;
                    $.each(latestCompanyJobPostList, function (index, company) {
                        $(".latest-companies").append('<div class="col-md-2 col-sm-4 col-xs-6 latest-company p-relative " onclick="OnCompanyDetail('+company.CompanyID+')">'
                                                        + '<div class="latest-company-inner">' 
                                                            + '<a>'
                                                                + '<div class="company-logo">'
                                                                    + '<img src="Images/Company/' + company.LOGO + '" />'
                                                                + '</div>'
                                                                + '<div class="company-name">' + company.CompanyName + '</div>'
                                                            + '</a>'
                                                        + '</div>'
                                                     + '</div>');
                    });
                },
                error: function () {
                    
                }

            });
        }

        function OnCompanyDetail(ID) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetActiveJobByCompany",
                data: '{id:' + ID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                success: function (data) {
                    var activeJobs = data.d;
                    $(".company-detail").empty();
                    $(".category-detail").empty();
                    $.each(activeJobs, function (index, activeJob) {
                        $(".company-detail").html('<div id="content" class="container-fluid site-width">'
                                + '<div class="col-ms-12">'
                                    + '<div class="header">'
                                        + '<h1 class="page-title pull-left text-primary"><span><b>' + activeJob.CompanyName + '</b></span></h1>'
                                    + '</div><div class="clearfix"></div >'
                                        + '<div class="company" id="company">'
                                            + '<div class="company-details row">'
                                                + '<div class="col-md-4 col-sm-6">'
                                                    + '<div class="description">'
                                                    + '</div>'
                                                    + '<div class="company-details">'
                                                        + '<div class="company-contact">'
                                                            + '<table class="table table-bordered">'
                                                                + '<tbody >'
                                                                    + '<tr>'
                                                                       + '<td colspan="2" class="alignC">'
                                                                          + '<img src="Images/Company/' + activeJob.Logo + '" style="width: 25%; margin-left: 35%;" />'
                                                                       + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                       + '<td class="caption"><span class="glyphicon glyphicon-envelope"></span></td>'
                                                                       + '<td><span class="value"><a href="#">'+activeJob.Email+'</a></span></td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                        + '<td class="caption"><span class="glyphicon glyphicon-phone"></span></td>'
                                                                        + '<td><span class="value"><a href="tel:012873713" class="phone-op op-mobitel">'+activeJob.PhoneNumber+'</a></span></td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                        + '<td class="caption"><span class="glyphicon glyphicon-map-marker"></span></td>'
                                                                        + '<td><span class="value">'+activeJob.Location+'</span>'
                                                                        + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                        + '<td class="caption"><span class="glyphicon glyphicon-globe"></span></td>'
                                                                        + '<td>'
                                                                            + '<span class="value"><a href="http://www.hkland.com" rel="nofollow" target="_blank">'+activeJob.WebSite+'</a></span><br>'
                                                                        + '</td>'
                                                                    + '</tr>'
                                                                    + '<tr>'
                                                                        + '<td class="caption"><span class="glyphicon glyphicon-paperclip"></span></td>'
                                                                        + '<td>'
                                                                            + '<p class="description pull-left"><span>' + activeJob.Descript + '</span></p>'
                                                                        + '</td>'
                                                                    + '</tr>'
                                                                + '</tbody>'
                                                            + '</table>'
                                                        + '</div >'
                                                    + '</div >'
                                                + '</div >'
                                                + '<div class="col-md-8 col-sm-6">'
                                                    + '<div class="active-jobs">'
                                                        
                                                    + '</div>'
                                                + '</div>' /*d*/
                                            + '</div>'
                                        + '</div>'
                                      + '<div class="clearfix"></div>'
                                    + '</div>'
                            + '</div>');
                    });
                    $.each(activeJobs, function (index, activeJob) {
                        $(".active-jobs").append('<div class="div-header text-success">Active jobs</div>'
                                                        + '<div class="job">'
                                                        + '<div class="job_detail_page" onclick="JobDetailPage(' + activeJob.Job_ID + ')">'
                                                            + '<div class="row">'
                                                                + '<div class="job-position col-md-6">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Position :</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="value position-title"><a href="#" target="_blank">'+activeJob.Position+'</a></span>'
                                                                                + '<br>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                                + '<div class="job-category col-md-6">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Categories :</span>'
                                                                                + '<span class="glyphicon glyphicon-briefcase">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="value"><a href="#">'+activeJob.Category+'</a></span>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>'
                                                            + '<div class="row">'
                                                                + '<div class="job-salary col-md-6">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Salary :</span>'
                                                                                + '<span class="glyphicon glyphicon glyphicon-usd">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="value">'+activeJob.Salary+'</span>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                                + '<div class="job-closing-date col-md-6">'
                                                                     + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="text-danger">Closing Date :</span>'
                                                                                + '<span class="glyphicon glyphicon glyphicon-time">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="text-danger">'+activeJob.ExpireDate+'</span>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>'
                                                            + '<div class="row">'
                                                                + '<div class="job-salary col-md-12">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Location :</span>'
                                                                                + '<span class="glyphicon glyphicon-map-marker">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<a href="#">'+activeJob.Province+'</a>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>'
                                                        + '</div>'
                                                    + '</div>'
                                                    //react - mount - point - unstable
                                                    + '<div class="row">'
                                                          + '<div class="col-md-12">'
                                                                    + '<div class="col-md-12 group-user-react">'
                                                                        + '<div class="col-md-2 d4u">'
                                                                            + '<div class="user-react" onclick="On0LkLike(' + activeJob.Job_ID + ')">'
                                                                                + '<div class="_8vbLike' + activeJob.Job_ID + '">'
                                                                                        + ' <span class="fa fa-thumbs-o-up" style="font-size: 130%; margin-left: 15%;"></span> <b>Like</b>'
                                                                                + '</div>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                        + '<div class="col-md-1  num0b_like num0b' + activeJob.Job_ID + '">'
                                                                            + '<div class="hrem8" data-toggle="tooltip" data-placement="bottom" title="Like!">'
                                                                                + '<a class="num4r8_like num4r8' + activeJob.Job_ID + '" style="color: #606770;"></a>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                        + '<div class="col-md-3 d4u">'
                                                                            + '<div class="user-react" onclick="OnUpload(' + activeJob.Job_ID + ')">'
                                                                                + '<div class="_8vbUpload' + activeJob.Job_ID + '">'
                                                                                    + ' <span class="fa fa-paperclip" style="font-size: 130%; margin-left: 12%;"></span> <b>Upload CV</b>'
                                                                                + '</div>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                        + '<div class="col-md-3 d4u">'
                                                                            + '<div class="user-react" onclick="On0svBookMark(' + activeJob.Job_ID + ')">'
                                                                                + '<div class="_8vbBookmark' + activeJob.Job_ID + '">'
                                                                                    + ' <span class="fa fa-bookmark-o" style="font-size: 130%; margin-left: 15%;"></span> <b>Bookmark</b>'
                                                                                + '</div>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                             + '</div>'
                                                      + '</div>' //end user react
                                                 + '</div>'
                                                );
                        likeStatus(activeJob.Job_ID);
                        countJobLike(activeJob.Job_ID);
                    });
                },
                error: function () {
                    console.log("error");
                }
            });
        }

        function GetJobByCategoryList()
        {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetJobByCategoryList",
                contentType: "application/json; charset=utf-8",
                data: {},
                datatype: "json",
                success: function (response) {
                    var categoryList = response.d;
                    $.each(categoryList, function (index, category) {
                        $(".group-category-list").append('<div class="col-xs-6 col-sm-3 col-md-4 list-container" onclick="OnJobByCategoryDetail(' + category.CategoryID + ')">'
                            + '<a href="#" class="list-group-item"><span class="cname">' + category.CategoryName + '</span>'
                            + '<span class="badge">' + category.availablePosition + '</span>'
                            +'</a></div>' );
                    });
                }
            });
        }

        function OnJobByCategoryDetail(ID)
        {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetActiveJobByCategoryDetail",
                data: '{category_id:' + ID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                success: function (data) {
                    var activeJobs = data.d;
                    $(".category-detail").empty();
                    $(".company-detail").empty();
                    $.each(activeJobs, function (index, activeJob) {
                        $(".category-detail").append('<div id="content" class="container-fluid site-width">'
                                + '<div class="col-md-12">'
                                    + '<div class="header">'
                                    + '</div><div class="clearfix"></div >'
                                        + '<div class="company" id="company">'
                                            + '<div class="company-details row">'
                                                + '<div class="col-md-3 col-sm-6">'
                                                    + '<div class="company-details">'
                                                        + '<div class="company-contact">'
                                                            + '<table class="table table-bordered">'
                                                                + '<tbody >'
                                                                    + '<tr>'
                                                                       + '<td colspan="2" class="alignC text-center">'
                                                                            + '<img src="Images/Company/' + activeJob.LOGO + '" style="width: 50%;" /><br>'
                                                                            + '   <span><b><a href="#">' + activeJob.CompanyName + '</a></b></span>'
                                                                        + '</td>'
                                                                    + '</tr>'
                                                                + '</tbody>'
                                                            + '</table>'
                                                        + '</div >'
                                                    + '</div >'
                                                + '</div >'
                                                + '<div class="col-md-9 col-sm-6">'
                                                    + '<div class="active-jobs">' //Go to job detail here
                                                        + '<div class="job">'
                                                            + '<div class="job_detail_page" onclick="JobDetailPage(' + activeJob.ID + ')">'
                                                            + '<div class="row">'
                                                                + '<div class="job-position col-md-6">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Position :</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="value position-title"><a href="#" target="_blank">'+activeJob.Position+'</a></span>'
                                                                                + '<br>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                                + '<div class="job-category col-md-6">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Categories :</span>'
                                                                                + '<span class="glyphicon glyphicon-briefcase">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="value"><a href="#">'+activeJob.Category+'</a></span>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>'
                                                            + '<div class="row">'
                                                                + '<div class="job-salary col-md-6">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Salary :</span>'
                                                                                + '<span class="glyphicon glyphicon glyphicon-usd">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="value">'+activeJob.Salary+'</span>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                                + '<div class="job-closing-date col-md-6">'
                                                                     + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="text-danger">Closing Date :</span>'
                                                                                + '<span class="glyphicon glyphicon glyphicon-time">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell" >'
                                                                                + '<span class="text-danger">'+activeJob.ExpiredOn+'</span>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>'
                                                            + '<div class="row">'
                                                                + '<div class="job-salary col-md-12">'
                                                                    + '<div class="div-table">'
                                                                        + '<div class="div-row">'
                                                                            + '<div class="div-cell">'
                                                                                + '<span class="caption">Location :</span>'
                                                                                + '<span class="glyphicon glyphicon-map-marker">&nbsp;</span>'
                                                                            + '</div>'
                                                                            + '<div class="div-cell">'
                                                                                + '<a href="#">'+activeJob.Province+'</a>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>'
                                                          + '</div>'
                                                            + '<hr />'

                                                            //react - mount - point - unstable
                                                            + '<div class="row">'
                                                                + '<div class="col-md-12">'
                                                                    + '<div class="col-md-12 group-user-react">'
                                                                        + '<div class="col-md-2 d4u">'
                                                                            + '<div class="user-react" onclick="On0LkLike(' + activeJob.ID + ')">'
                                                                                + '<div class="_8vbLike' + activeJob.ID + '">'
                                                                                        + ' <span class="fa fa-thumbs-o-up" style="font-size: 130%; margin-left: 15%;"></span> <b>Like</b>'
                                                                                + '</div>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                        + '<div class="col-md-1  num0b_like num0b' + activeJob.ID + '">'
                                                                            + '<div class="hrem8" data-toggle="tooltip" data-placement="bottom" title="Like!">'
                                                                                + '<a class="num4r8_like num4r8'+activeJob.ID+'" style="color: #606770;"></a>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                        + '<div class="col-md-3 d4u">'
                                                                            + '<div class="user-react" onclick="OnUpload(' + activeJob.ID + ')">'
                                                                                + '<div class="_8vbUpload'+activeJob.ID+'">'
                                                                                    + ' <span class="fa fa-paperclip" style="font-size: 130%; margin-left: 12%;"></span> <b>Upload CV</b>'
                                                                                + '</div>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                        + '<div class="col-md-3 d4u">'
                                                                            + '<div class="user-react" onclick="On0svBookMark('+activeJob.ID+')">'
                                                                                + '<div class="_8vbBookmark'+activeJob.ID+'">'
                                                                                    + ' <span class="fa fa-bookmark-o" style="font-size: 130%; margin-left: 15%;"></span> <b>Bookmark</b>'
                                                                                + '</div>'
                                                                            + '</div>'
                                                                        + '</div>'
                                                                    + '</div>'
                                                                + '</div>'
                                                            + '</div>' //end user react


                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                                + '</div>'

                                            + '</div>'
                                        + '</div>'
                                      + '<div class="clearfix"></div>'
                                    + '</div>'
                                + '</div>'
                        );
                        likeStatus(activeJob.ID);
                        countJobLike(activeJob.ID);
                    });
                },
               
            });
        }
        function GetUserReationPOST() {
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetUserReationPOST",
                contentType: "application/json; charset=utf-8",
                data: {},
                datatype: "json",
                success: function (response) {
                    var ReationInfo = response.d;
                    $(".ipsDataList_reducedSpacing").empty();
                    $.each(ReationInfo, function (index, info) {
                        var strAcc = "";
                        if (info.Profiles != "") {
                            strAcc = '<img src="Images/User/' + info.Profiles + '" />'
                        } else {
                            strAcc = '<i class="fa fa-user-circle-o" style="font-size: 240%;"></i>';
                        }
                        $(".ipsDataList_reducedSpacing").append('<li class="ipsDataItem">'
                               + '<div class="ipsDataItem_icon ipsPos_top">'
                                  + '<a href="#" class="ipsUserPhoto ipsUserPhoto_tiny" >'
                                    + strAcc
                                  + '</a>'
                               + '</div>'
                               + '<div class="ipsDataItem_main cWidgetComments">'
                                  + '<div class="ipsCommentCount ipsPos_right " >' + info.LikeAmount + '</div>'
                                     + '<div class="ipsType_break ipsContained">'
                                       + '<a href="#" style="white-space: nowrap"><span><span class="ipsBadge" style="background: #00a1ff;">Like</span></span></a>'
                                       + '<a href="#" class="ipsDataItem_title">' + info.Position + '</a>'
                                     + '</div>'
                                       + '<p class="ipsType_reset ipsType_medium ipsType_blendLinks ipsContained">'
                                       + '<span>By '
                                       + '<a href="#" class="ipsType_break">' + info.User + '</a></span><br>'
                                       + '<span class="ipsType_light">Started <time datetime="2019-10-10T04:02:29Z" title="10/10/19 04:02  AM" data-short="Oct 10">' + info.CreatedOn + '</time></span>'
                                       + '</p>'
                                + '</div>'
                              + '</li>'

                        );
                    });

                    
                }
            });
        }

        function OnSearch()
        {
            var category = $('#<%=DrCategory.ClientID%>').val();
            var position = $('#<%=DrPosition.ClientID%>').val();
            var location = $('#<%=DrLocation.ClientID%>').val();
            var salary = $('#<%=DrSalary.ClientID%>').val();

            var list = { category: category, pos: position, locat: location, sal: salary };
            var data = JSON.stringify(list);

            $.ajax({
                type: "POST",
                url: "Default.aspx/OnSearch",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var JobList = result.d;
                        $(".latest-companies").empty();
                        $(".group-category-list").empty();
                        $(".company-detail").empty();
                        $(".category-detail").empty();
                        $(".lastestJobByCompany").empty();
                        $(".job-search-filter-result").empty();
                    $(".hdpanel").empty();
                    $(".numresult").empty();
                    if (JobList != 0) {
                        var count = 0;
                        $.each(JobList, function (index, Job) {
                            $(".job-search-filter-result").append('<div class="job">'
                               + '<div class="job-inner">'
                                    + '<div class="job-company hidden-xs">'
                                        + '<div class="company-name mainf">'
                                            + '<a href="#">'
                                                + '<img src="Images/Company/' + Job.LOGO + '" class="img-responsive company-logo" /></a>'
                                                + '   <span class="value"><a href="#">' + Job.CompanyName + '</a></span>'
                                        + '</div>'
                                    + '</div>'

                                    + '<div class="job-details">'
                                      + '<div class="job_detail_page" onclick="JobDetailPage(' + Job.ID + ')">'
                                        + '<div class="row">'
                                           + ' <div class="job-salary col-md-6">'
                                              + '  <div class="div-table">'
                                                 + '   <div class="div-row">'
                                                    + '    <div class="div-cell">'
                                                       + '     <span class="caption">Position :</span>'
                                                          + '  <span class="glyphicon glyphicon glyphicon-briefcase">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                           + ' <span class="value">' + Job.Position + '</span>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'

                                            + '<div class="job-jobtype col-md-6">'
                                               + ' <div class="div-table">'
                                                 + '   <div class="div-row">'
                                                   + '     <div class="div-cell">'
                                                      + '      <span class="caption">Type :</span>'
                                                         + '   <span class="glyphicon glyphicon glyphicon-time">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                          + '  <span class="value">' + Job.JobType + '</span>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'

                                            + ' <div class="job-salary col-md-6">'
                                              + '  <div class="div-table">'
                                                 + '   <div class="div-row">'
                                                    + '    <div class="div-cell">'
                                                       + '     <span class="caption">Salary :</span>'
                                                          + '  <span class="glyphicon glyphicon glyphicon-usd">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                           + ' <span class="value">' + Job.Salary + '</span>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'

                                            + '<div class="job-Priority col-md-6">'
                                               + ' <div class="div-table">'
                                                  + '  <div class="div-row">'
                                                     + '   <div class="div-cell">'
                                                        + '    <span class="caption">Priority :</span>'
                                                           + ' <span class="glyphicon glyphicon-list-alt">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                          + '  <a>' + Job.Priority + '</a>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'

                                        + '</div>'

                                        + '<div class="row">'
                                            + '<div class="job-category col-md-6">'
                                             + '   <div class="div-table">'
                                                + '    <div class="div-row">'
                                                 + '       <div class="div-cell">'
                                                    + '        <span class="caption">Categories :</span>'
                                                      + '      <span class="glyphicon glyphicon-tags">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                          + '  <span class="value"><a class="category-link">' + Job.Category + '</a></span>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'

                                            + '<div class="job-closing-date col-md-6">'
                                               + ' <div class="div-table">'
                                                 + '   <div class="div-row">'
                                                   + '     <div class="div-cell">'
                                                      + '      <span class="text-danger">Closing Date :</span>'
                                                         + '   <span class="glyphicon ">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                          + '  <span class="text-danger">' + Job.ExpiredOn + '</span>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'

                                            + '<div class="job-location col-md-6">'
                                               + ' <div class="div-table">'
                                                  + '  <div class="div-row">'
                                                     + '   <div class="div-cell">'
                                                        + '    <span class="caption">Location :</span>'
                                                           + ' <span class="glyphicon glyphicon-map-marker">&nbsp;</span>'
                                                        + '</div>'
                                                        + '<div class="div-cell">'
                                                          + '  <a>' + Job.Province + '</a>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'
                                            
                                          + '</div>' //end of detail page
                                                
                                        + '</div>'
                                        + '<hr />'
                                        //react - mount - point - unstable
                                            + '<div class="row">'
                                                + '<div class="col-md-12">'
                                                    + '<div class="col-md-2"></div>'
                                                    + '<div class="col-md-9 group-user-react">'
                                                        + '<div class="col-md-2 d4u">'
                                                            + '<div class="user-react" onclick="On0LkLike(' + Job.ID + ')">'
                                                                + '<div class="_8vbLike' + Job.ID + '">'
                                                                        + ' <span class="fa fa-thumbs-o-up" style="font-size: 130%; margin-left: 24%;"></span> <b>Like</b>'
                                                                + '</div>'
                                                            + '</div>'
                                                        + '</div>'
                                                        + '<div class="col-md-1  num0b_like num0b' + Job.ID + '">'
                                                            + '<div class="hrem8">'
                                                                + '<a class="num4r8_like num4r8'+Job.ID+'" style="color: #606770;"></a>'
                                                            + '</div>'
                                                        + '</div>'
                                                        + '<div class="col-md-3 d4u">'
                                                            + '<div class="user-react" onclick="OnUpload(' + Job.ID + ')">'
                                                                + '<div class="_8vbUpload'+Job.ID+'">'
                                                                    + ' <span class="fa fa-paperclip" style="font-size: 130%; margin-left: 21%;"></span> <b>Upload CV</b>'
                                                                + '</div>'
                                                            + '</div>'
                                                        + '</div>'
                                                        + '<div class="col-md-3 d4u">'
                                                            + '<div class="user-react" onclick="On0svBookMark('+Job.ID+')">'
                                                                + '<div class="_8vbBookmark'+Job.ID+'">'
                                                                    + ' <span class="fa fa-bookmark-o" style="font-size: 130%; margin-left: 23%;"></span> <b>Bookmark</b>'
                                                                + '</div>'
                                                            + '</div>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>' //end user react
                                    + '</div>'
                                + '</div>'
                                + ' </div>'
                            );
                            likeStatus(Job.ID);
                            countJobLike(Job.ID);
                            count++;
                        });
                        $(".numresult").append('<p class="text-center">Show all job '+ count +'</p>');
                    }
                    else
                    {
                        $(".lastestJobByCompany").append("No job found!");
                    }
                }
            });
        }
        function likeStatus(jobid) {
            var userid = $('#<%=UserID.ClientID%>').val();
            if (userid != "") {
                var list = { username: userid, job_id: jobid };
                var data = JSON.stringify(list);
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/JobLikeStatus",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == 0) {
                            $("._8vbLike" + jobid).css({ color: "#333" });
                        }
                        else {
                            $("._8vbLike" + jobid).css({ color: "rgb(32, 120, 244)" });
                        }
                    }
                });
            } else {
                $("._8vbLike" + jobid).css({ color: "#333" });
            }
                                                        
        }
        function countJobLike(ID) {
            var list = { job_id: ID };
            var data = JSON.stringify(list);
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/countJobLike",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var numLike = response.d;
                        if (numLike != 0) {
                            $(".num4r8" + ID).html('<i class="fa fa-thumbs-up" aria-hidden="true" style="color: rgb(32, 120, 244);"></i> ' + numLike);
                        } else {
                            $(".num4r8" + ID).empty();
                        }
                    }
                });
        }
        function On0LkLike(ID)
        {
            var status = "Like failed! You're not login!";
            var username = $('#<%=UserID.ClientID%>').val();
            if (username != "") {
                var list = { username: username, job_id: ID };
                var data = JSON.stringify(list);
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/JobLikeStatus",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == 0) {
                            $.ajax({
                                type: "POST",
                                url: "Default.aspx/OnJobLike",
                                data: data,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    if (response.d != 0) {
                                        $("._8vbLike" + ID).css({ color: "rgb(32, 120, 244)" });
                                        countJobLike(ID);
                                        GetUserReationPOST();
                                    } else {
                                        countJobLike(ID);
                                        GetUserReationPOST();
                                    }
                                }
                            });
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "Default.aspx/OnJobUnLike",
                                data: data,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    if (response.d != 0) {
                                        $("._8vbLike" + ID).css({ color: "#333" });
                                        countJobLike(ID);
                                        GetUserReationPOST();
                                    } else {
                                        $("._8vbLike" + ID).css({ color: "rgb(32, 120, 244)" });
                                        countJobLike(ID);
                                        GetUserReationPOST();
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                alertify.set('notifier', 'position', 'top-right');
                alertify.error('<i class="fa fa-info-circle" aria-hidden="true" style="font-size: 23px; color: rgb(62, 130, 245);"></i> '+ status);
            } 
            
        }
     
        //$("._8vbUpload" + ID).css({ color: "rgb(32, 120, 244)" });
        
        function OnUpload(ID)
        {
            var username = $('#<%=UserName.ClientID%>').val();
            var UserID = $('#<%=UserID.ClientID%>').val();
            var status = "Upload failed! You're not login!";
            if (username != "") {
                $('#<%=JID.ClientID%>').val(ID);
                $('#<%=UID.ClientID%>').val(UserID);
                $('#<%=recipientName.ClientID%>').val(username);

                //$("#UploadModalLabel").html('Apply CV to ' + '<b class="text-info">' + 'IT Programmer Officer' + '</b>');
                $("#UploadModalLabel").html('Apply CV');
                ShowModal();
            }
            else {
                alertify.set('notifier', 'position', 'top-right');
                alertify.error('<i class="fa fa-info-circle" aria-hidden="true" style="font-size: 23px; color: rgb(62, 130, 245);"></i> ' + status);
            }
        }

        function validate_file() {
            var file_err = 'file_err';
            var upload_cv = $('#upload_cv');
            var file = $('#upload_cv')[0].files[0]
            //hide previous error
            $("#" + file_err).html("");

            if (file == undefined) {
                upload_cv.parent().after('<span id=' + file_err + '><p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i> Please upload CV(.doc, .docx, .pdf) File</p></span>');
            } else {
                
            }

            var fileType = file.type; // holds the file types
            var match = ["application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]; // defined the file types
            var fileSize = file.size;
            var maxSize = 2 * 1024 * 1024;

            // Checking the Valid Image file types  
            if (!((fileType == match[0]) || (fileType == match[1]))) {
                upload_cv.val("");
                upload_cv.parent().after('<span id=' + file_err + '><p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i> Please select a valid (.doc, .docx, .pdf) file.</p></span>');
                return false;
            } else {
                $("#" + file_err).html("");
            }
            // Checking the defined image size
            if (fileSize > maxSize) {
                upload_cv.val("");
                upload_cv.parent().after('<span id=' + file_err + '><p class="text-danger"><i class="fa fa-times" aria-hidden="true"></i> Please select a file less than 2 MB of size.</p></span>');
                return false;
            } else {
                $("#" + file_err).html("");
            }
        }

        function OnSendCV()
        {
            var file_name = $("#upload_cv").val();
            var FileName = file_name.substring(file_name.lastIndexOf('\\') + 1);
            if (FileName != "") {
                var job_id = $('#<%=JID.ClientID%>').val();
                var user_id = $('#<%=UID.ClientID%>').val();
                var username = $('#<%=recipientName.ClientID%>').val();
                var list = { job_id: job_id, user_id: user_id, file_cv: FileName };
                var data = JSON.stringify(list);

                $.ajax({
                    type: "POST",
                    url: "Default.aspx/OnSendCV",
                    data: data,
                    contentType: "application/json; charset=utf-8;",
                    dataType: "JSON",
                    success: function (response) {
                        var result = response.d;
                        if (result != false) {
                            $("._8vbUpload" + job_id).css({ color: "rgb(32, 120, 244)" });
                        } else {
                            $("._8vbUpload" + job_id).css({ color: "#333" });
                        }
                    }

                });
            }

        }

        function On0svBookMark(ID) {
            var status = "Mark failed! You're not login!";
            var username = $('#<%=UserName.ClientID%>').val();
            if (username != "") {
                $("._8vbBookmark" + ID).css({ color: "rgb(32, 120, 244)" });
            }
            else {
                alertify.set('notifier', 'position', 'top-right');
                alertify.error('<i class="fa fa-info-circle" aria-hidden="true" style="font-size: 23px; color: rgb(62, 130, 245);"></i> ' + status);
            }
        }

        function JobDetailPage(ID) {
            var list = { job_id: ID };
            var data = JSON.stringify(list);
            $.ajax({
                type: "POST",
                url: "Pages/Jobs/Job_Detail.aspx/JobDetailPage",
                data: data,
                contentType: "application/json; charset=utf-8",
                success: function () {
                    window.location.href = "Pages/Jobs/Job_Detail.aspx?job_id=" + ID;
                }
            });
        }

        function ShowModal()
        {
            $("#btnShowPopup").click();
        }

        
    </script>
   
    <br />
    <div class="container-fluid">
        <br />
        <br />
        
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading panel-info">
                    <h3 class="panel-title "><b>Search Job</b></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:DropDownList ID="DrCategory" runat="server" Style="padding-left: 7px; height: 10%;" class="form-control" AppendDataBoundItems="True" DataSourceID="SqlDataSourceCategory" DataTextField="Category_Name" DataValueField="Category_ID">
                                <asp:ListItem Selected="True" Value="0">--All Category--</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSourceCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Category ORDER BY Category_Name ASC "></asp:SqlDataSource>

                        </div>
                        <div class="col-lg-2">
                            <asp:DropDownList ID="DrPosition" runat="server" Style="padding-left: 7px; height: 10%;" class="form-control" AppendDataBoundItems="True" DataSourceID="SqlDataSourcePosition" DataTextField="Position_Name" DataValueField="Position_ID">
                                <asp:ListItem Selected="True" Value="0">--All Fields--</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSourcePosition" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Position ORDER BY Position_Name ASC "></asp:SqlDataSource>

                        </div>
                        <div class="col-lg-2">
                            <asp:DropDownList ID="DrLocation" runat="server" Style="padding-left: 7px; height: 10%;" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceLocation" DataTextField="Province_Name" DataValueField="Province_ID">
                                <asp:ListItem Selected="True" Value="0">--All Location--</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSourceLocation" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * From JP_TBL_Province ORDER BY Province_Name ASC "></asp:SqlDataSource>
                        </div>
                        <div class="col-lg-2">
                            <asp:DropDownList ID="DrSalary" runat="server" Style="padding-left: 7px; height: 10%;" class="form-control" AppendDataBoundItems="true" DataSourceID="SqlDataSourceSalary" DataTextField="Salary" DataValueField="Salary_ID">
                                <asp:ListItem Selected="True" Value="0">--All Salary--</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSourceSalary" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM JP_TBL_Salary ORDER BY Salary_ID ASC"></asp:SqlDataSource>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="btn btn-sm btn-primary" onClick="OnSearch()"><i class="fa fa-search"></i>Search</button>
                        </div>
                        <div class="col-lg-2" style="display:none">
                              <asp:TextBox ID="UserID" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-2" style="display:none">
                              <asp:TextBox ID="UserName" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="search">
            <%--Job search filter result--%>
            <div class="row job-search-filter-result" style="margin-right: 0px; margin-left: 0px;">

            </div>
            <br />
            <div class="numresult" style="color: red; font-weight: bold;">

            </div>
            <button type="button" style="display: none;" id="btnShowPopup" class="btn btn-primary btn-lg"
                   data-toggle="modal" data-target="#UploadModal">
            </button>

            <div class="modal fade" id="UploadModal" tabindex="-1" role="dialog" aria-labelledby="UploadModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="UploadModalLabel"></h4>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="recipient-name" class="control-label">Recipient:</label>
                          <asp:TextBox ID="JID" runat="server" disabled="disabled" class="form-control" style="display: none"></asp:TextBox>
                          <asp:TextBox ID="UID" runat="server" disabled="disabled" class="form-control" style="display: none"></asp:TextBox>
                          <asp:TextBox ID="recipientName" runat="server" disabled="disabled" class="form-control"></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <label for="message-text" class="control-label">Application Form (CV):</label>
                          <input type="file" class="form-control" id="upload_cv" name="upload_cv" accept=".doc,.docx,.pdf" onchange="validate_file();">
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="OnSendCV()">Send CV</button>
                  </div>
                </div>
              </div>
            </div>

            <%-- Display 6 Company Urgent Post --%>
            <div class="lastestJobByCompany text-center">
                <h4 class="text-info"><b>Latest Jobs by Companies</b></h4>
            </div>
            <div class="row latest-companies">

            </div>
            <br />

            <div class="row company-detail">
                
            </div>
            <div class="row hdpanel">
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Jobs By Category
                            <div class="pull-right"></div>
                        </div>
                        <div class="panel-body">
                            <div class="row group-category-list">

                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="row category-detail">
                      
                    </div>


                    <hr />
                    <br />
                </div>
                <div class="col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="text-center" style="margin-left:20%;"><b>POPULAR JOB NOW</b></span>
                            <div class="pull-right"></div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="ipsPad_half ipsWidget_inner">
                                    <ul class="ipsDataList ipsDataList_reducedSpacing">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>

    </div>
</asp:Content>
