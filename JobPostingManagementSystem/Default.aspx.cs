﻿using JobPostingManagementSystem.Dashboard.Company;
using JobPostingManagementSystem.Dashboard.Jobs;
using JobPostingManagementSystem.Dashboard.Manage_Post.Category;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UserID.Text = Session["UserID"].ToString();
                UserName.Text = Session["Username"].ToString(); 
            }
            catch (Exception)
            {
            }
        }

        [WebMethod]
        public static List<bl_company>GetCompanyList()
        {
            List<bl_company> comList = new List<bl_company>();
            comList = da_company.GetCompanyList();

            return comList;
        }

        [WebMethod]
        public static List<bl_job> GetLatestCompanyJobPostList()
        {
            List<bl_job> jobList = new List<bl_job>();
            jobList = da_job.GetLatestCompanyJobPostList();

            return jobList;
        }

        [WebMethod]
        public static List<bl_category> GetJobByCategoryList()
        {
            List<bl_category> categoryList = new List<bl_category>();
            categoryList = da_category.GetAvailableJobByCategory();

            return categoryList;    
        }

        //company filter detail
        [WebMethod]
        public static List<bl_company> GetActiveJobByCompany(string id)
        {
            List<bl_company> activeJob = new List<bl_company>();
            activeJob = da_company.GetActiveJobByCompany(id);

            return activeJob;
        }

        [WebMethod]
        public static List<bl_job> GetActiveJobByCategoryDetail(string category_id)
        {
            List<bl_job> activeJob = new List<bl_job>();
            activeJob = da_job.GetActiveJobByCategoryDetail(category_id);

            return activeJob;
        }

        [WebMethod]
        public static List<bl_job> OnSearch(string category, string pos, string locat, string sal)
        {
            List<bl_job> jobSearchList = new List<bl_job>();
            jobSearchList = da_job.GetJobSearch(category, pos, locat, sal);

            return jobSearchList;

        }

        [WebMethod]
        public static int JobLikeStatus(string username, string job_id)
        {
            int JosStatus = 0;
            JosStatus = da_job.JobLikeStatus(username, job_id);

            return JosStatus;

        }

         [WebMethod]
        public static int OnJobLike(string username, string job_id)
        {
             int JosStatus = 0;
             JosStatus = da_job.OnJobLike(username, job_id);

             return JosStatus;

        }
        [WebMethod]
         public static int OnJobUnLike(string username, string job_id)
        {
             int JosStatus = 0;
             JosStatus = da_job.OnJobUnLike(username, job_id);

             return JosStatus;

        }
        [WebMethod]
        public static int countJobLike(string job_id)
        {
            int numLike = 0;
            DataTable dt = new DataTable();
            dt = da_job.countJobLike(job_id);
            numLike = dt.Rows.Count;
            return numLike;

        }
        [WebMethod]
        public static bool OnSendCV(string job_id, string user_id, string file_cv)
        {
            bool status = false;
            status = da_job.OnSendCV(job_id, user_id, file_cv);
            if (status != false)
            {
                string save_path = "~/Uploads/";
                string sourceFile = @"C:\Users\Public\Documents\Eyes Health document.docx";
                string desFile = HttpContext.Current.Server.MapPath(save_path + file_cv);
                File.Copy(sourceFile, desFile, true);
            }
            return status;
        }

        [WebMethod]
        public static List<bl_job> GetUserReationPOST()
        {
            List<bl_job> ReationInfo = new List<bl_job>();
            ReationInfo = da_job.GetReationInfo();

            return ReationInfo;

        }

    }
}