﻿using JobPostingManagementSystem.App_Code.DA.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem
{
    public partial class View_profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AccountImage.ImageUrl = "Images/" + Session["Profiles"].ToString();
                lblUname.Text = Session["Fullname"].ToString();
                txtUsrname.Text = Session["Username"].ToString();
                txtEmail.Text = Session["Email"].ToString();
            }
            catch (Exception)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string userid, username, email;
            userid = Session["UserID"].ToString();
            username = txtUsrname.Text.Trim();
            email = txtEmail.Text.Trim();
            bool status = da_users.UpdateUser(userid, username, email);
            if (status != false)
                Response.Redirect("~/Default.aspx");
            else
                lblResult.Text = "Udpate Profile failed!";
        }
    }
}