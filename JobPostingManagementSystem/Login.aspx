﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Login.aspx.cs" Inherits="JobPostingManagementSystem.Login.Login" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
    <script type="text/javascript">

        
    </script>
    <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title text-left text-primary"><b>User Login</b></h3>
                        </div>
                        <div class="panel-body">
                                <fieldset>
                                    <div class="form-group">
                                        <asp:TextBox ID="txtusername" runat="server" class="form-control" placeholder="Username" ></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox type="password" ID="txtpassword" runat="server" class="form-control" placeholder="Password" ></asp:TextBox>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <asp:CheckBox ID="chkRememberMe" runat="server" />Remember me on this 
                                        </label>
                                    </div>
                                    <asp:Button ID="btnLogin" runat="server" class="btn btn-primary btn-block" Style="border: 1pt groove #d5d5d5; Width: 100%" Text="Login" ValidationGroup="search" ToolTip="Search User" OnClick="btnLogin_Click" />
                                </fieldset>
                        </div>
                        <div class="login-help" style="text-align: center">
                            <p><a href="#">Reset password</a> Still cannot login? <a href="#">Feedback us.</a></p>
                            <a href="User_register.aspx">
                                <b><i class="fa fa-fw fa-edit"></i> New User Register Here</b>
                            </a>
                        </div>
                        <br />
                        <div class="ErrorLogin" style="text-align: center">
                            <p>
                                <asp:Label ID="lblResult" runat="server" Text="" Style="text-align: center;" ForeColor="Red"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>