﻿using JobPostingManagementSystem.App_Code.DA.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Login
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string username, password, IsAdmin;
                username = txtusername.Text.Trim();
                password = txtpassword.Text.Trim();
                if (username != "" && password != "")
                {
                    DataTable users = da_users.GetUserLogIn(username, password);
                    if (users.Rows.Count > 0)
                    {
                        foreach (DataRow dr in users.Rows)
                        {
                            Session["UserID"] = dr["UserID"].ToString();
                            Session["Fullname"] = dr["Fullname"].ToString();
                            Session["Username"] = dr["Username"].ToString();
                            Session["Email"] = dr["Email"].ToString();
                            Session["Profiles"] = dr["Profiles"].ToString();
                            IsAdmin = dr["RoleID"].ToString();
                            if (IsAdmin == "001")
                                Session["RoleID"] = "Admin";
                            else
                                Session["RoleID"] = "Guest";
                            Response.Redirect("~/Default.aspx");
                        }
                    }
                    else
                    {
                        lblResult.Text = "Login failed. Please try again"; 
                    }
                }
                else
                {
                    lblResult.Text = "Please check username and password again!";
                }
            }
            catch (Exception)
            {
                lblResult.Text = "System error, please contact administrator!";
            }
        }
    }
}