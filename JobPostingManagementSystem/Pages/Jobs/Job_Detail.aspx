﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/Site.Master" CodeBehind="Job_Detail.aspx.cs" Inherits="JobPostingManagementSystem.Pages.Jobs.Job_Detail" %>

<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <style>
        .caption {
            width: 30%;
        }

    </style>
    <div class="container">
        <br />
        <br />
        <div id="content" class="container-fluid site-width">
            <div class="col-ms-12">
                <div class="header">
                    <h1 class="page-title pull-left">
                        <span>
                            <asp:Label ID="lblPosition" Text="test" runat="server" /> for <a href="#company"><asp:Label ID="lblCompany" Text="" runat="server" /></a></span>
                    </h1>
                </div>
                <div class="clearfix"></div>


                <div class="content">
                    <div class="job">
                        <div class="row">
                            <div class="col-sm-7 col-md-8  col-lg-9">
                                <div class="row job-detail">
                                    <div class="col-md-6">
                                        <table class="table table-bordered column-left">
                                            <tbody>
                                                <tr>
                                                    <td class="caption"><span class="caption">Categories :</span></td>
                                                    <td><span class="value"><a href="/category/70-computer-science"><asp:Label ID="lblCategory" Text="test" runat="server" /></a></span></td>
                                                </tr>
                                                <tr>
                                                    <td class="caption"><span class="caption">Schedule :</span></td>
                                                    <td><span class="value"><a href="/full-time-jobs"><asp:Label ID="lblJobType" Text="test" runat="server" /></a></span></td>
                                                </tr>
                                                <tr>
                                                    <td class="caption"><span class="caption">Sex :</span></td>
                                                    <td><span class="value">Male, Female</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="caption"><span class="caption">Location :</span></td>
                                                    <td>
                                                        <asp:Label ID="lblLocation" Text="test" runat="server" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-bordered column-right">
                                            <tbody>
                                                <tr>
                                                    <td class="caption"><span class="caption">Closing Date :</span></td>
                                                    <td><span class="value"><asp:Label ID="lblExpireDate" Text="" runat="server" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td class="caption"><span class="caption">Salary :</span></td>
                                                    <td><span class="value"><asp:Label ID="lblSalary" Text="" runat="server" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td class="caption"><span class="caption">Experience :</span></td>
                                                    <td>
                                                        <span class="value"></span>
                                                        At least 3 years                                                            </td>
                                                </tr>
                                                <tr>
                                                    <td class="caption"><span class="caption">Degree :</span></td>
                                                    <td>
                                                        <span class="value">Master</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="panel panel-default company-details" id="company">
                                    <div class="panel-heading">
                                        Company(<a href="#">Opening jobs</a>)
                                    </div>
                                    <div class="panel-body row">
                                            <div class="col-md-12 company-desc">
                                            <img src="https://www.khmeronlinejobs.com/images/slogo/intean-poalroath-rongroeurng-ltd.jpg" class="img-responsive pull-left logo-j-details">
                                        </div>

                                        <div class="col-md-8">
                                            <table class="table">
                                                <tbody>
                                                    <tr class="company-name">
                                                        <td class="caption"><span class="glyphicon glyphicon-home"></span><span class="caption">Company :</span></td>
                                                        <td><span class="value"><a href="/company/712-intean-poalroath-rongroeurng-ltd?ref=j" target="_blank">Intean Poalroath Rongroeurng Ltd.</a></span></td>
                                                    </tr>
                                                    <tr class="company-email">
                                                        <td class="caption"><span class="glyphicon glyphicon-envelope"></span><span class="caption">Email :</span></td>
                                                        <td><span class="value"><a href="mailto:hof@iprmfi.com">hof@iprmfi.com</a><br>
                                                            <a href="mailto:info@iprmfi.com">info@iprmfi.com</a></span></td>
                                                    </tr>
                                                    <tr class="company-mobile">
                                                        <td class="caption"><span class="glyphicon glyphicon-phone"></span><span class="caption">Mobile :</span></td>
                                                        <td><span class="value"><a href="tel:010520000" class="phone-op op-smart">010 520 000</a></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption"><span class="glyphicon glyphicon-map-marker"></span><span class="caption">Address :</span></td>
                                                        <td><span class="value">#779A, Kampuchea Kraom Blvd, Sangkat Tuek Laak 1, Khan Tuol Kouk, Phnom Penh.</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="caption"><span class="glyphicon glyphicon-globe"></span><span class="caption">Website :</span></td>
                                                        <td>
                                                            <span class="value"><a href="http://www.iprmfi.com" rel="nofollow" target="_blank">http://www.iprmfi.com</a></span><br>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4 map">
                                            <a href="http://maps.google.com/?q=11.566393403312556,104.89199727773666" target="_blank">
                                                <img src="https://maps.googleapis.com/maps/api/staticmap?center=11.566393403312556,104.89199727773666&amp;zoom=15&amp;size=296x200&amp;maptype=roadmap&amp;markers=color:red%7C11.566393403312556,104.89199727773666&amp;key=AIzaSyBYMgNOLl9tCIp2hLEcSq01CMmSplCRRaE&amp;signature=PxFRfL0eaE7q36_QGlvnCw84vD0=" data-rel="11.566393403312556,104.89199727773666" alt="Intean Poalroath Rongroeurng Ltd. map" class="img-responsive static-map" style=""></a>
                                            <br>
                                            <a href="http://maps.google.com/?q=11.566393403312556,104.89199727773666" target="_blank">Click on map to view bigger Google map                             </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End company -->
                                
                            </div>
                            <div class="col-sm-5 col-md-4 col-lg-3">
                                <div class="panel panel-default panel-info jobs-widget" id="interested-jobs">
                                    <div class="panel-heading">Similar jobs</div>
                                    <div class="panel-body row">
                                        <div class="col-md-12">
                                            <div class="alignC" id="loadingImage" style="display: none;">
                                                <img src="/images/loading.gif" class="loading" alt="Loading"></div>
                                            <div id="relatedJobs" style="">
                                                <div class="jobs related-jobs ">
                                                    <div class="related-job">
                                                        <div class="position-title">
                                                            <a href="/job/31400-Publicity and Creative and design artists (Job Code: 5002)?ref=s">Publicity and Creative and design artists (Job Code: 5002)</a>
                                                        </div>
                                                        <div class="position-number">
                                                            <span class="glyphicon glyphicon-user g-icon"></span>: 2 positions               
                                                        </div>
                                                        <div class="company-name">
                                                            <span class="glyphicon glyphicon-home g-icon"></span>: <a href="/company/3559-east-asia-management-university?ref=s">East Asia Management University</a>
                                                        </div>
                                                        <div class="salary"><span class="glyphicon glyphicon-usd g-icon"></span>: $300 - $600</div>
                                                    </div>
                                                    <div class="divider-1"></div>
                                                    <div class="related-job">
                                                        <div class="position-title">
                                                            <a href="/job/31397-Procurement officer for Grant and Analyst?ref=s">Procurement officer for Grant and Analyst</a>
                                                        </div>
                                                        <div class="position-number">
                                                            <span class="glyphicon glyphicon-user g-icon"></span>: 1 position               
                                                        </div>
                                                        <div class="company-name">
                                                            <span class="glyphicon glyphicon-home g-icon"></span>: <a href="/company/314-world-vision-cambodia?ref=s">World Vision Cambodia</a>
                                                        </div>
                                                        <div class="salary"><span class="glyphicon glyphicon-usd g-icon"></span>: $749 - $937</div>
                                                    </div>
                                                    <div class="divider-2 divider-even"></div>
                                                    <div class="related-job last">
                                                        <div class="position-title">
                                                            <a href="/job/31360-IT Technician?ref=s">IT Technician</a>
                                                        </div>
                                                        <div class="position-number">
                                                            <span class="glyphicon glyphicon-user g-icon"></span>: 2 positions               
                                                        </div>
                                                        <div class="company-name">
                                                            <span class="glyphicon glyphicon-home g-icon"></span>: <a href="/company/3501-ptm-travel-tours-co-ltd?ref=s">PTM Travel &amp; Tours Co., Ltd</a>
                                                        </div>
                                                        <div class="salary"><span class="glyphicon glyphicon-usd g-icon"></span>: Negotiable</div>
                                                    </div>
                                                    <div class="divider-3"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</asp:Content>
