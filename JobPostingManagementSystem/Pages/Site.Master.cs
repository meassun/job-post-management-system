﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobPostingManagementSystem.Pages
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Username"] != "")
                {
                    lblIsUserLogedIn.Text = Session["Username"].ToString();
                    lblUsername.Text = Session["Username"].ToString();
                    lblUserIsAdmin.Text = Session["RoleID"].ToString();
                    AccountImage.ImageUrl = "../Images/" + Session["Profiles"].ToString();
                }

            }
            catch (Exception)
            {
            }
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("~/Default.aspx");
        }
    }
}