﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="User_register.aspx.cs" Inherits="JobPostingManagementSystem.User_register" %>

<asp:Content ID="Register_User" runat="server" ContentPlaceHolderID="MainContent">
   <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
    <script type="text/javascript">
        //$(document).ready(function () {
        //    $.ajax({
        //        type: "POST",
        //        url: "Site.Master/GetUserLoginName",
        //        data: '{"'+ Session["Username"] + '"}',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (data) {
        //            alert("Success");
        //        },
        //        failure: function (response) {
        //            alert("Failures!");
        //        }
        //    });
        //});
        
    </script>
    <div class="container">
        <div class="row" id="Register_panel" runat="server">
            <div class="col-md-6 col-md-offset-3">
                    <br />
                    <br />
                    <br />
                    <br />
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-left"><b>Registeration</b></h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <asp:TextBox ID="txtFname" runat="server" class="form-control" placeholder="Full name..." ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtusername" runat="server" class="form-control" placeholder="Username..." ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email..." ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtPassword" type="password" runat="server" class="form-control" placeholder="Password..." ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtConPassword" type="password" runat="server" class="form-control" placeholder="Confirm Password..." ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:fileupload ID="FileUpload" runat="server" xmlns:asp="#unknown" />
                            </div>
                            <div class="col-md-6">
                                <asp:Button ID="btnSave" runat="server" class="btn btn-primary btn-block pull-right" Style="border: 1pt groove #d5d5d5; Width: 50%" Text="Save" ValidationGroup="Save" ToolTip="Add User" OnClick="btnSave_Click" />
                            </div>
                            <div class="col-md-6">
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-block pull-left" Style="border: 1pt groove #d5d5d5; Width: 50%" Text="Cancel" ValidationGroup="Cancel" ToolTip="Cancel" OnClick="btnCancel_Click" />
                            </div>
                        
                        </fieldset>
                    </div>
                    <div class="login-help" style="text-align: center">
                            <a href="Login.aspx">
                                <p>Already have an account?</p>
                                <b><i class="fa fa-sign-in"></i><a href="Login.aspx"> Log In</a></b>
                            </a>
                        </div>
                        <br />
                    <div class="ErrorLogin" style="text-align: center">
                        <asp:Label ID="lblResult" runat="server" Text="" Style="text-align: center;" ForeColor="Red"></asp:Label>
                    </div>
                    <br />
                </div>
            </div>
        </div>

        <%--Success Registered Panel--%>
        <div class="row" id="Success_Register_panel" runat="server">
            <br /><br /><br /><br />
            <div class="col-md-12 ">
                <div class="regist-panel  panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center text-uppercase" style="font-weight:bold;">Registeration</h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <p>Dear <asp:Label ID="lblUserName" runat="server" Text="" Style="text-align: center; font-weight: bold"></asp:Label><br>
                                        <br>
                                        Your personal settings have been registered.</p>
                                    <p>An e-mail has been sent to remind you of your login and password.</p>
                                    <a href="Default.aspx">Search Job</a>
                                            <br>
                                            <br>
                                </div>
                            </div>
                        
                        </fieldset>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
