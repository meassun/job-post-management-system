﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JobPostingManagementSystem.App_Code;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using JobPostingManagementSystem.App_Code.DA.Users;
using System.IO;

namespace JobPostingManagementSystem
{
    public partial class User_register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Success_Register_panel.Attributes.CssStyle.Add("display", "none");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string fullName, userName, email, password, conpassword, Image, Role;
            string save_path = "~/Images/";
            fullName = txtFname.Text.Trim();
            userName = txtusername.Text.Trim();
            email = txtEmail.Text.Trim();
            password = txtPassword.Text.Trim();
            conpassword = txtConPassword.Text.Trim();
            Image = Path.GetFileName(FileUpload.PostedFile.FileName);
            if (Image != "")
                FileUpload.PostedFile.SaveAs(Server.MapPath(save_path + Image));
            Role = "002"; //Visitor user --User Public registered.
            if (userName != "" && email != "" && password != "" && conpassword != "")
            {
                if (password == conpassword)
                {
                   //Check existing user registered
                   if (da_users.GetUserExistingRegister(userName, email) == false)
                   {
                        bool status = false;
                        status = da_users.AddNewUser(fullName, userName, email, password, Image, Role);
                        if (status == true)
                        {
                            Page_Load(this, null);
                            Register_panel.Attributes.CssStyle.Add("display", "none");
                            Success_Register_panel.Attributes.CssStyle.Add("display", "block");
                            lblUserName.Text = userName;
                            clearSession(fullName, userName, email, password, Image, Role);
                            Session["Username"] = userName;
                        }
                        else
                        {
                            clearSession(fullName, userName, email, password, Image, Role);
                            lblResult.Text = "Registered failed!";
                        }
                    }
                   else
                   {
                        lblResult.Text = "Username is already exist!";
                   }
                }
                else
                {
                    lblResult.Text = "Password is not match!";
                }

            }
            else 
            {
                clearSession(fullName, userName, email, password, Image, Role);
                lblResult.Text = "Please check username and password again!";
            }
           
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        { 
            
        }
        void clearSession(string fullName, string userName, string email, string pass, string image, string role) {
            fullName = "";
            userName = "";
            email = "";
            pass = "";
            image = "";
            role = "";
        }
    }
}